/*
 * Developed by Terekhov Roman on 07.09.18 17:01.
 * Last modified 07.09.18 16:59.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import org.apache.commons.cli.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandLineParserCustom {
    private static final Logger logger = Logger.getLogger (CommandLineParserCustom.class.getName ());

    public static CommandLine getArgs(String[] args) {
        CommandLineParser parser = new DefaultParser ();
        Options options = new Options ();
        HelpFormatter formatter = new HelpFormatter ();
        CommandLine cmd = null;
        Option ip = new Option ("ip", "ip", true, "IP - address for multicast sending");
        Option port = new Option ("port", "port", true, "port for multicast sending");

        ip.setRequired (true);
        port.setRequired (true);

        options.addOption (ip);
        options.addOption (port);

        try {
            cmd = parser.parse (options, args);
        } catch (ParseException e) {
            logger.log (Level.WARNING, e.getMessage ());
            formatter.printHelp ("lab1", options);
            cmd = null;
        }
        return cmd;
    }
}

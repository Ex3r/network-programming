package ru.ccfit.terekhov.model.messages;

import ru.ccfit.terekhov.model.messages.Message;

import static ru.ccfit.terekhov.other.Constatnts.DELIMETER_IN_PACKET;
import static ru.ccfit.terekhov.other.Constatnts.USUAL_MSG;

public class UsualMessage implements Message {
    private final int type = USUAL_MSG;
    private String uuid;
    private String message;

    public UsualMessage(String uuid, String message) {
        this.uuid = uuid;
        this.message = message;
    }

    @Override
    public String toString() {
        return type + DELIMETER_IN_PACKET + uuid + DELIMETER_IN_PACKET + message;
    }

    @Override
    public byte[] toByteArray() {
        return toString ().getBytes ();
    }

    @Override
    public int getTypeOfMessage() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }
}

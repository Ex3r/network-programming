package nsu.ru.chat.client;

import nsu.ru.chat.client.commands.AbstractCommand;

import java.util.HashMap;

class CommandsHolder {
    private HashMap<String, AbstractCommand> commands;

    CommandsHolder() {
        commands = new HashMap<>();
    }

    void addCommand(String commandName, AbstractCommand command) {
        if (!commands.containsKey(commandName)) {
            commands.put(commandName, command);
        }
    }

    HashMap<String, AbstractCommand> getCommands() {
        return commands;
    }
}

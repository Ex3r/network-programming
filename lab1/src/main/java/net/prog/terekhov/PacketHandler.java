/*
 * Developed by Terekhov Roman on 18.09.18 1:34.
 * Last modified 18.09.18 1:29.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import net.prog.terekhov.model.Data;

import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PacketHandler implements Runnable {
    private static final Logger logger = Logger.getLogger (PacketHandler.class.getName ());
    private ConcurrentHashMap<String, Data> storage;
    private static final int INIT_VALUE_FOR_COUNTER = 0;
    private static final int DEAD_VALUE_FOR_COUNTER = 16;
    private static final int DELAY = 1000;


    PacketHandler() {
        this.storage = new ConcurrentHashMap<> ();
    }

    public void handlePacket(DatagramPacket packet) throws UnsupportedEncodingException {
        String uuid = new String (packet.getData (), "UTF-8");
        String hostAddress = packet.getAddress ().getHostAddress ();
        //если нет в коллекции, то добавить и вывести список всех
        if (!storage.containsKey (uuid)) {
            Data temp = new Data (hostAddress, INIT_VALUE_FOR_COUNTER);
            storage.put (uuid, temp);
            logger.info ("added new element to storage: " + uuid);
            printIpAndAmountOfCopy ();
        } else {
            //если есть, то обнулить счётчик
            storage.get (uuid).nullifyCounter ();
        }
    }


    private void checkTimeoutAndDelete() {
        storage.forEach ((k, v) -> {
            if ((v.getCounter () + 1) < DEAD_VALUE_FOR_COUNTER) {
                v.incrementCounter ();
            } else {
                storage.remove (k);
                logger.info ("deleted element from storage: " + k);
                printIpAndAmountOfCopy ();
            }
        });
    }

    private void printIpAndAmountOfCopy() {
        StringBuilder info = new StringBuilder ("Alive IPs: \n");
        storage.forEach ((k, v) -> {
            info.append (v.getIp ()).append ("\n");
        });
        info.append ("Amount of copy: ").append (storage.size ()).append (" \n");
        System.out.println (info);
    }

    @Override
    public void run() {
        while (!Thread.currentThread ().isInterrupted ()) {
            checkTimeoutAndDelete ();
            try {
                Thread.sleep (DELAY);
            } catch (InterruptedException e) {
                logger.log (Level.WARNING, e.getLocalizedMessage ());
            }
        }
    }
}

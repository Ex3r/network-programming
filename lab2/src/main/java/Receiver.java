import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.Socket;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class Receiver extends Thread {
    private static final Logger logger = Logger.getLogger (Receiver.class.getName ());
    private static final String UPLOADS_DIR_PATH = "uploads" + File.separator;
    private static final int BUFFER_SIZE = 1024;
    private Socket clientSocket;
    private DataInputStream in;
    private DataOutputStream out;


    public Receiver(Socket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            in = new DataInputStream (clientSocket.getInputStream ());
            out = new DataOutputStream (clientSocket.getOutputStream ());

        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    private boolean checkFileSizeAfterTransfering(long length, String pathToFile) {
        File file = new File (pathToFile);
        long tempLength = file.length ();
        return tempLength == length;
    }

    private void deleteBrokenFile(String pathToDirectory) throws IOException {
        FileUtils.deleteDirectory (new File (pathToDirectory));
        logger.info ("Directory " + pathToDirectory + " was deleted");
    }

    @Override
    public void run() {
        byte[] buffer = new byte[BUFFER_SIZE];
        String uuid = null;
        String newDirectoryPath = null;
        try {
            uuid = in.readUTF ();

            //creating new folder
            newDirectoryPath = UPLOADS_DIR_PATH + uuid;
            Utilits.createFolder (newDirectoryPath);

            String readFilename = in.readUTF ();
            String shortFileName = Paths.get (readFilename).getFileName ().toString ();
            String newFilePath = newDirectoryPath + File.separator + shortFileName;
            File newFile = new File (newFilePath);
            boolean success = newFile.createNewFile ();

            if (success) {
                logger.info ("File " + newFilePath + " created successfully");
            } else {
                printStateOfDownloading (readFilename, uuid, false, "can not create new file " + newFilePath);
                clientSocket.close ();
                in.close ();
                out.close ();
            }


            FileOutputStream fileOutputStream = new FileOutputStream (newFile);
            long fileSize = in.readLong ();

            //замер скорости
            SpeedTest speedTest = new SpeedTest ();
            speedTest.start (fileSize, uuid);

            long byteRead = 0;
            while (byteRead < fileSize) {
                int read = in.read (buffer);
                speedTest.check (read);
                fileOutputStream.write (buffer, 0, read);
                byteRead += read;
            }


            if (checkFileSizeAfterTransfering (fileSize, newFilePath)) {
                out.writeUTF ("Успешно");
                speedTest.finish ();
            } else {
                deleteBrokenFile (newDirectoryPath);
            }


            fileOutputStream.close ();
            clientSocket.close ();
            in.close ();
            out.close ();
        } catch (IndexOutOfBoundsException ex) {
            logger.info ("Клиент неожиданно отключился id: " + uuid);
            try {
                deleteBrokenFile (newDirectoryPath);
            } catch (IOException e) {
                logger.severe ("error when deleting broken file " + e.getMessage ());
            }
        } catch (IOException e) {
            logger.severe (e.getMessage ());
        }
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    private void printStateOfDownloading(String filename, String clientUUID, boolean isSuccess, String error) {
        if (isSuccess) {
            System.out.printf ("File %s was successfully downloaded from client %s%n", filename, clientUUID);
        } else {
            System.out.printf ("Downloading of file %s  from client %s not ended because of error %s%n", filename, clientUUID, error);
        }
    }


}
package ru.nsu.terekhov.client;

import java.io.IOException;

public class ClientStarter {


    public static void main(String[] args) throws IOException {
        InputReader inputReader = new InputReader();
        inputReader.start();
    }
}

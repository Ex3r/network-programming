package ru.ccfit.terekhov;


import javafx.util.Pair;
import ru.ccfit.terekhov.main.Input;
import ru.ccfit.terekhov.main.PingService;
import ru.ccfit.terekhov.main.Receiver;
import ru.ccfit.terekhov.main.Sender;
import ru.ccfit.terekhov.model.PacketToSend;
import ru.ccfit.terekhov.model.messages.HelloMessage;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class Node {
    private static final Logger logger = Logger.getLogger (Node.class.getName ());
    private final String nodeName;
    private int packetLossPercent;
    private Integer port;
    private int parentPort;
    private InetAddress parentIp;


    private DatagramSocket socket;
    private Sender sender;
    private Receiver receiver;
    private Input input;
    private PingService pingService;
    private boolean isRoot = false;

    private Set<Pair<InetAddress, Integer>> neighbours = new HashSet<> ();
    private Map<String, PacketToSend> taskStorage = new ConcurrentHashMap<> ();


    public Node(String nodeName, int packetLossPercent, int port) throws SocketException, UnknownHostException {
        this.nodeName = nodeName;
        this.packetLossPercent = packetLossPercent;
        this.port = port;
        socket = new DatagramSocket (port);
        isRoot = true;

        parentIp = null;
        parentPort = -1;
    }

    public Node(String nodeName, int packetLossPercent, int port, InetAddress parentIp, int parentPort) throws SocketException, UnknownHostException {
        this.nodeName = nodeName;
        this.packetLossPercent = packetLossPercent;
        this.port = port;
        socket = new DatagramSocket (port);
        isRoot = false;
        this.parentPort = parentPort;
        this.parentIp = parentIp;

        //добавляем информацию о родителе
        neighbours.add (new Pair<> (parentIp, parentPort));

    }


    public void start() {
        sender = new Sender (socket, neighbours, taskStorage);
        pingService = new PingService (sender);
        receiver = new Receiver (socket, packetLossPercent, sender, pingService);
        input = new Input (sender);

        if (!isRoot) {
            String id = UUID.randomUUID ().toString ();
            logger.info ("Положили hello packet with id " + id);
            sender.sendHelloMessage (new HelloMessage (id), new Pair<> (parentIp, parentPort));
        }

        Thread threadForInput = new Thread (input);
        Thread threadForSender = new Thread (sender);
        Thread threadForReceive = new Thread (receiver);
        Thread threadForPingService = new Thread (pingService);

        threadForInput.start ();
        threadForSender.start ();
        threadForReceive.start ();
        threadForPingService.start ();

    }


}

package nsu.ru.chat.client.commands;

import nsu.ru.chat.client.ClientInfo;

import javax.websocket.Session;
import java.io.IOException;
import java.util.logging.Logger;

public class LogoutCommand implements AbstractCommand {
    private static final Logger log = Logger.getLogger(LogoutCommand.class.getName());

    @Override
    public void execute() {
        if (!ClientInfo.isLogined()) {
            System.out.println("Вы не вошли в чат , чтобы из него выходить");
            return;
        }


        Session session = ClientInfo.getSession();
        //TODO maybe will work
        try {
            session.close();
            ClientInfo.resetInfo();
            System.out.println("Вы вышли из чата, можете залогиться под старым именем если он будет свободно");
        } catch (IOException e) {
            log.severe("Ошибка закрытия сессии, причина: " + e.getMessage());
        }

    }
}

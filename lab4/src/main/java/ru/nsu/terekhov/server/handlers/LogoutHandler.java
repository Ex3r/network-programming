package ru.nsu.terekhov.server.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.dto.UserLogoutResponseDto;
import ru.nsu.terekhov.common.TokenStore;
import ru.nsu.terekhov.common.ExchangeClass;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import static ru.nsu.terekhov.responses.HttpStatusCodes.StatusCodes.*;

@SuppressWarnings("Duplicates")
public class LogoutHandler implements HttpHandler, AbstractHandler {
    private static final Logger logger = Logger.getLogger(LogoutHandler.class.getName());
    private ConcurrentHashMap<String, User> users;

    public LogoutHandler(ConcurrentHashMap<String, User> users) {
        this.users = users;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        logger.info("user trying to logout");
        ExchangeClass exchangeClass = validateRequest(exchange);
        Gson gson = new Gson();
        boolean ok = exchangeClass.isOk();
        if (!ok) {
            writeResponse(exchange, "");
            return;
        }

        HttpPrincipal principal = exchange.getPrincipal();
        String token = principal.getRealm();
        User user = TokenStore.findUserByToken(token, users);

        //200
        addHeaderToResponse(exchange, "Content-type", "application/json");
        String json = gson.toJson(new UserLogoutResponseDto("bye!"));
        exchange.sendResponseHeaders(OK.getCode(), json.length());
        writeResponse(exchange, json);
        users.remove(user.getUsername());
        logger.info("user " + user.getUsername() + " has been disconnected");


    }

    @SuppressWarnings("Duplicates")
    @Override
    public ExchangeClass validateRequest(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        if (!requestMethod.equals("POST")) {
            exchange.sendResponseHeaders(METHOD_NOT_ALLOWED.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }
        return new ExchangeClass(exchange, true, null);
    }


}

package ru.ccfit.terekhov.model.messages;



import static ru.ccfit.terekhov.other.Constatnts.ACK_MSG;
import static ru.ccfit.terekhov.other.Constatnts.DELIMETER_IN_PACKET;

public class AcknowledgeMessage implements Message {
    private final int type = ACK_MSG;
    private String uuid;

    public AcknowledgeMessage(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
            return type + DELIMETER_IN_PACKET + uuid;
    }

    @Override
    public byte[] toByteArray() {
        return toString ().getBytes ();
    }

    @Override
    public int getTypeOfMessage() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }
}

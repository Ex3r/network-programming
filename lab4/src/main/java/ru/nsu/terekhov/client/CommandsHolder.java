package ru.nsu.terekhov.client;

import ru.nsu.terekhov.client.commands.AbstractCommand;

import java.util.HashMap;

public class CommandsHolder {
    private HashMap<String, AbstractCommand> commands;

    public CommandsHolder() {
        commands = new HashMap<>();
    }

    public void addCommand(String commandName, AbstractCommand command) {
        if (!commands.containsKey(commandName)) {
            commands.put(commandName, command);
        }
    }

    public HashMap<String, AbstractCommand> getCommands() {
        return commands;
    }
}

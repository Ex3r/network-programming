package ru.nsu.terekhov.client;

import ru.nsu.terekhov.dto.Message;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.dto.UserResponseDto;

import java.util.LinkedList;

public class ClientInfo {
    private static String username;
    private static int id;
    private static String token="";
    private static boolean online=false;
    private static LinkedList<Message> messages = new LinkedList<>();
    private static LinkedList<UserResponseDto> userList = new LinkedList<>();




    public static boolean isOnline() {
        return online;
    }

    public static  void setUserInfo(User user) {
        username = user.getUsername();
        token = user.getToken();
        id = user.getId();
        online = user.getOnline();
    }

    public static  void clearUserInfo() {
        username = "";
        token = "";
        id = -1;
        online = false;
    }

    public static  void clearUserList() {
        userList.clear();
    }

    public static void updateUserList(LinkedList<UserResponseDto> users) {
        ClientInfo.userList = users;
    }

    public static  void addUserToUserList(UserResponseDto user) {
        userList.add(user);
    }

    public static void addGottedMessages(LinkedList<Message> messages) {
        LinkedList<Message> newMessages = new LinkedList<>();
        newMessages.addAll(messages);

        messages.removeAll(ClientInfo.messages);
        messages.forEach(message ->  {
            if (message.getAuthor() != ClientInfo.id) {
                System.out.println(findUsernameById(message.getAuthor()) + " " + message.getMessage());
            }
        });

        if (newMessages.size()!=0) {
            ClientInfo.messages.addAll(newMessages);
        }
    }


    public static boolean containsUserId(int userId) {
        if (findUserById(userId) != null) {
            return true;
        } else {
            return false;
        }
    }

    private static  String findUsernameById(int id) {
        UserResponseDto user = findUserById(id);
        if (user !=null) {
            return user.getUsername();
        } else {
            return "ProjectX";
        }
    }

    private static  UserResponseDto findUserById(int id) {
        UserResponseDto userResponse = null;
        for (UserResponseDto user : userList) {
            if (user.getId() == id) {
                userResponse = user;
                break;
            }
        }
        return userResponse;
    }

    public static String getToken() {
        return token;
    }

    public static String getUsername() {
        return username;
    }

    public static  int getCurrentAmountOfMessages () {
        return ClientInfo.messages.size();
    }

    public static LinkedList<UserResponseDto> getUserList() {
        return userList;
    }

    public static void setUserList(LinkedList<UserResponseDto> userList) {
        ClientInfo.userList = userList;
    }
}

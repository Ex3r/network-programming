import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandLineParserCustom {
    private List<Option> optionList;
    private Options options;
    private CommandLineParser parser;
    private HelpFormatter formatter;
    private CommandLine cmd = null;
    private static final Logger logger = Logger.getLogger (CommandLineParserCustom.class.getName ());

    public CommandLineParserCustom(Map<String, String> parametersForOptions) {
        if (parametersForOptions == null) {
            throw new IllegalArgumentException ("parametersForOptions mustn't be null");
        }
        optionList = new LinkedList<> ();
        parser = new DefaultParser ();
        options = new Options ();
        formatter = new HelpFormatter ();


        parametersForOptions.forEach ((option, description) -> {
            Option temp = new Option (option, option, true, description);
            temp.setRequired (true);
            options.addOption (temp);
            optionList.add (temp);
        });
    }

    public CommandLine getArgs(String[] args) {
        try {
            cmd = parser.parse (options, args);
        } catch (ParseException e) {
            logger.log (Level.WARNING, e.getMessage ());
            formatter.printHelp ("lab2", options);
            cmd = null;
        }
        return cmd;
    }
}

package ru.nsu.terekhov.server;

import com.sun.net.httpserver.*;
import ru.nsu.terekhov.dto.Message;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.server.handlers.LoginHandler;
import ru.nsu.terekhov.server.handlers.LogoutHandler;
import ru.nsu.terekhov.server.handlers.MessageHandler;
import ru.nsu.terekhov.server.handlers.UsersHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import static ru.nsu.terekhov.common.TokenStore.findUserByToken;
import static ru.nsu.terekhov.responses.HttpStatusCodes.StatusCodes.*;

class Server {
    private static Logger logger = Logger.getLogger(Server.class.getName());
    private ConcurrentHashMap<String, User> users;
    private ConcurrentLinkedQueue<Message> messages;

    Server() throws IOException {
        users = new ConcurrentHashMap<>();
        messages = new ConcurrentLinkedQueue<>();
        Authenticator authenticator = new Authenticator() {
            @Override
            public Result authenticate(HttpExchange exch) {
                Headers requestHeaders = exch.getRequestHeaders();
                String authorization = requestHeaders.getFirst("Authorization");
                if (authorization == null) {
                    return new Failure(NOT_ACCEPTABLE.getCode());
                }

                List<String> contentTypeStrings = requestHeaders.get("Authorization");
                AtomicBoolean notFound = new AtomicBoolean(true);
                contentTypeStrings.forEach(string -> {
                    if (string.contains("Token")) {
                        notFound.set(false);
                    }
                });
                if (notFound.get()) {
                    return new Failure(UNAUTHORIZED.getCode());
                }

                String token;
                try {
                    token = authorization.substring(6);
                } catch (IndexOutOfBoundsException ex) {
                    return new Failure(UNAUTHORIZED.getCode());
                }
                User user = findUserByToken(token, users);

                if (user == null) {
                    return new Failure(FORBIDDEN.getCode());
                } else {
                    return new Success(new HttpPrincipal(user.getUsername(), user.getToken()));
                }
            }
        };
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 100);
        server.createContext("/login", new LoginHandler(users));
        server.createContext("/logout", new LogoutHandler(users)).setAuthenticator(authenticator);
        server.createContext("/users", new UsersHandler(users)).setAuthenticator(authenticator);
        server.createContext("/messages", new MessageHandler(users, messages)).setAuthenticator(authenticator);
        server.setExecutor(null);
        server.start();
        logger.info("Server started");
    }

    void start(String[] args) throws Exception {

    }
}

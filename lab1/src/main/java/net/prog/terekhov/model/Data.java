/*
 * Developed by Terekhov Roman on 07.09.18 17:01.
 * Last modified 07.09.18 16:59.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov.model;

public class Data {
    private String ip;
    private int counter;

    public Data(String ip, Integer counter) {
        this.ip = ip;
        this.counter = counter;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public void incrementCounter() {
        counter++;
    }

    public void nullifyCounter() {
        counter = 0;
    }
}

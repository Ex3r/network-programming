/*
 * Developed by Terekhov Roman on 18.09.18 1:34.
 * Last modified 18.09.18 1:32.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MulticastValidator {
    public static boolean validateIp(String Ip) throws UnknownHostException {
        InetAddress inet = InetAddress.getByName (Ip);
        return inet.isMulticastAddress ();
    }
}

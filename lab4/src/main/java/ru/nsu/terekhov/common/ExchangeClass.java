package ru.nsu.terekhov.common;

import com.sun.net.httpserver.HttpExchange;

public class ExchangeClass {
    private HttpExchange exchange;
    private boolean ok;
    private String response;

    public ExchangeClass(HttpExchange exchange, boolean ok, String response) {
        this.exchange = exchange;
        this.ok = ok;
        this.response = response;
    }

    public HttpExchange getExchange() {
        return exchange;
    }

    public boolean isOk() {
        return ok;
    }

    public String getResponse() {
        return response;
    }
}

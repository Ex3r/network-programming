package ru.nsu.terekhov.client.commands;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.Message;
import ru.nsu.terekhov.dto.MessageSendRequestDto;
import ru.nsu.terekhov.dto.MessageSendResponseDto;
import ru.nsu.terekhov.dto.UserLogoutResponseDto;

public class MessageCommand{
    private ApiServiceInterface api = ApiServiceInterface.newInstance();


    public void execute(String message) {
        if (ClientInfo.getToken().isEmpty()) {
            System.out.println("Необходима авторизация для отправки сообщения, авторизуйтесь с помощью команды /login");
            return;
        }

        Call<MessageSendResponseDto> asyncResponse = api.postMessage("Token " + ClientInfo.getToken(), new MessageSendRequestDto(message));

        Callback<MessageSendResponseDto> callback = new Callback<MessageSendResponseDto>() {
            @Override
            public void onResponse(Call<MessageSendResponseDto> call, Response<MessageSendResponseDto> response) {
                switch (response.code()) {
                    case 403 : {
                        break;
                    }
                    case 200 : {
                        MessageSendResponseDto mes = response.body();
                        //ClientInfo.addMyMessageId(mes.getId());
                        break;
                    }
                    default: {
                        System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                    }
                }


            }

            @Override
            public void onFailure(Call<MessageSendResponseDto> call, Throwable throwable) {
                System.out.println("Ошибка при отправке запроса на сервер :" + throwable.getMessage());
            }
        };

        asyncResponse.enqueue(callback);
    }
}

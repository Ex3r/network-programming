package ru.nsu.terekhov.dto;

public class UserLogoutResponseDto {
    private String message;

    public UserLogoutResponseDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

package ru.nsu.terekhov.dto;

public class MessageSendResponseDto {
    private int id;
    private String message;

    public MessageSendResponseDto(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}

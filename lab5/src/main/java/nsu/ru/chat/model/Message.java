package nsu.ru.chat.model;


import java.io.Serializable;

public class Message implements Serializable {
    private String id;
    private String type;
    private String author;
    private String msg;


    public Message() {
    }


    public Message(String id, String type, String author, String msg) {
        this.id = id;
        this.type = type;
        this.author = author;
        this.msg = msg;
    }

    public Message(String type, String author, String msg) {
        this.type = type;
        this.author = author;
        this.msg = msg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

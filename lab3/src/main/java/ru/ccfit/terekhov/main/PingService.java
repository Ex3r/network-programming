package ru.ccfit.terekhov.main;

import javafx.util.Pair;

import java.net.InetAddress;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Thread.sleep;
import static ru.ccfit.terekhov.other.Constatnts.DEAD_VALUE;
import static ru.ccfit.terekhov.other.Constatnts.PING_INTERVAL;

public class PingService implements Runnable {
    private Sender sender;
    private Map<Pair<InetAddress, Integer>, Long> infoAboutNeighbours;


    public PingService(Sender sender) {
        this.sender = sender;
        this.infoAboutNeighbours = new ConcurrentHashMap<> ();
        Set<Pair<InetAddress, Integer>> neighbours = this.sender.getNeighbours ();
        neighbours.forEach ((neighbour) -> {
            infoAboutNeighbours.put (neighbour, ZonedDateTime.now ().toEpochSecond ());
        });
    }

    public void addNewNeighbour(Pair<InetAddress, Integer> newNeighbour) {
        infoAboutNeighbours.put (newNeighbour, ZonedDateTime.now ().toEpochSecond ());
    }

    public void removeNode(Pair<InetAddress, Integer> node) {
        infoAboutNeighbours.remove (node);
    }

    public void updateInfoAboutNode(Pair<InetAddress, Integer> newNeighbour) {
        infoAboutNeighbours.put (newNeighbour, ZonedDateTime.now ().toEpochSecond ());
    }

    private void sendPingAndCheckStatus() {
        infoAboutNeighbours.forEach ((node, lastTimeUpdate) -> {
            if ((ZonedDateTime.now ().toEpochSecond () - lastTimeUpdate) > DEAD_VALUE) {
                removeNode (node);
            } else {
                sender.sendPingMessage (node);
            }
        });
    }


    @Override
    public void run() {
        while (!Thread.currentThread ().isInterrupted ()) {
            try {
                sleep (PING_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace ();
            }
            sendPingAndCheckStatus ();
        }
    }
}

package ru.nsu.terekhov.dto;

public class UserLoginRequestDto {
    private String username;

    public UserLoginRequestDto(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}

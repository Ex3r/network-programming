package nsu.ru.chat.client;

import nsu.ru.chat.client.commands.LoginCommand;
import nsu.ru.chat.client.commands.LogoutCommand;
import nsu.ru.chat.client.commands.MessageCommand;
import nsu.ru.chat.client.commands.UsersCommand;
import org.glassfish.tyrus.client.ClientManager;

import java.util.Scanner;

class InputReader {
    private CommandsHolder commands = new CommandsHolder();
    private MessageCommand messageCommand = new MessageCommand();

    InputReader(ClientManager clientManager) {
        commands.addCommand("login", new LoginCommand(clientManager));
        commands.addCommand("logout", new LogoutCommand());
        commands.addCommand("users", new UsersCommand());
    }

    void start() {
        printUsage();
        Scanner input = new Scanner(System.in);
        String inputString;
        while (!Thread.currentThread().isInterrupted()) {
            inputString = input.nextLine();
            boolean isCommand = checkIsCommand(inputString);
            if (isCommand) {
                if (commands.getCommands().containsKey(inputString.substring(1))) {
                    commands.getCommands().get(inputString.substring(1)).execute();
                } else {
                    System.out.println("UNKNOWN COMMAND");
                }
            } else {
                messageCommand.execute(inputString);
            }
        }
    }

    private boolean checkIsCommand(String maybeCommand) {
        if (!maybeCommand.isEmpty()) {
            return maybeCommand.charAt(0) == '/';
        } else {
            return false;
        }
    }

    private void printUsage() {
        String description = "использование чата:\n /login - для того, чтобы авторизоваться в чате\n" +
                "/users -посмотреть список пользователей\n /logout - для того, чтобы выйти из чата\n";
        System.out.println(description);
    }
}

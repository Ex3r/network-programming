package nsu.ru.chat.client;

import org.glassfish.tyrus.client.ClientManager;

public class Client {


    public static void main(String[] args) throws Exception {
        ClientManager client = ClientManager.createClient();
        InputReader inputReader = new InputReader(client);
        inputReader.start();
    }


}
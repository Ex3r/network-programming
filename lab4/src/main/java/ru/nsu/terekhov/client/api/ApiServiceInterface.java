package ru.nsu.terekhov.client.api;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.*;
import ru.nsu.terekhov.dto.*;

public interface ApiServiceInterface {

    public static  ApiServiceInterface newInstance() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://localhost:8080/")
                .build().create(ApiServiceInterface.class);

    }

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<User> postUser(@Body UserLoginRequestDto username);

    @POST("logout")
    Call<UserLogoutResponseDto> postUserLogout(@Header("Authorization") String token);

    @GET("users")
    Call<UsersResponseDto> getUsers(@Header("Authorization") String token);

    @Headers("Content-Type: application/json")
    @POST("messages")
    Call<MessageSendResponseDto> postMessage(
            @Header("Authorization") String token,
            @Body MessageSendRequestDto messageSendRequestDto
    );

    @GET("messages")
    Call<MessagesResponseDto> getMessages(
            @Header("Authorization") String token,
            @Query("offset") Integer currentId,
            @Query("count") Integer count
    );
}
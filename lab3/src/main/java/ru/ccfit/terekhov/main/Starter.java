package ru.ccfit.terekhov.main;

import javafx.util.Pair;
import org.apache.commons.cli.CommandLine;
import ru.ccfit.terekhov.Node;
import ru.ccfit.terekhov.other.CommandLineParserCustom;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import static ru.ccfit.terekhov.other.Constatnts.*;

public class Starter {
    private static final Logger logger = Logger.getLogger (Starter.class.getName ());

    public static void main(String[] args) {
        //TODO свапнуть параметры и константы
        Map<String, Pair<Boolean, String>> parametersForOptions = new LinkedHashMap<> ();
        parametersForOptions.put ("name", new Pair<> (true, NODE_NAME_PARAMETER));
        parametersForOptions.put ("percent", new Pair<> (true, PACKET_LOSS_PARAMETER));
        parametersForOptions.put ("port", new Pair<> (true, SELF_PORT));
        parametersForOptions.put ("ip", new Pair<> (false, PARENT_IP));
        parametersForOptions.put ("parent_port", new Pair<> (false, PARENT_PORT));
        CommandLineParserCustom commandParser = new CommandLineParserCustom (parametersForOptions);
        CommandLine commandParserArgs = commandParser.getArgs (args);
        if (commandParserArgs == null) {
            return;
        }

        String nodeName = commandParserArgs.getOptionValue ("name");
        String packetLossPercentStr = commandParserArgs.getOptionValue ("percent");
        String selfPortStr = commandParserArgs.getOptionValue ("port");
        String ipStr = commandParserArgs.getOptionValue ("ip");
        String parentPortStr = commandParserArgs.getOptionValue ("parent_port");
        int selfPort;
        int packetLossPercent;
        try {

            if (((ipStr == null) && (parentPortStr != null)) || ((ipStr != null) && (parentPortStr == null))) {
                logger.info ("Неверные параметры: либо parent_port пустой , либо ip -> оба должны быть");
            } else if ((ipStr == null) && (parentPortStr == null)) {
                packetLossPercent = Integer.parseInt (packetLossPercentStr);
                selfPort = Integer.parseInt (selfPortStr);
                Node node = new Node (nodeName, packetLossPercent, selfPort);
                node.start ();
            } else {
                packetLossPercent = Integer.parseInt (packetLossPercentStr);
                selfPort = Integer.parseInt (selfPortStr);
                InetAddress ip = InetAddress.getByName (ipStr);
                int parentPort = Integer.parseInt (parentPortStr);
                Node node = new Node (nodeName, packetLossPercent, selfPort, ip, parentPort);
                node.start ();
            }
        } catch (SocketException e) {
            logger.severe ("Ошибка при создании узла: " + e.getMessage ());
        } catch (UnknownHostException e) {
            logger.severe (e.getMessage ());
        }
    }

}

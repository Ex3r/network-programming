package ru.nsu.terekhov.client.commands;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.UserResponseDto;
import ru.nsu.terekhov.dto.UsersResponseDto;

import java.util.LinkedList;

public class UsersCommand implements AbstractCommand {
    private ApiServiceInterface api = ApiServiceInterface.newInstance();


    @Override
    public void execute() {

        if (ClientInfo.getToken().isEmpty()) {
            return;
        }

        Call<UsersResponseDto> asyncResponse = api.getUsers("Token " + ClientInfo.getToken());

        Callback<UsersResponseDto> callback = new Callback<UsersResponseDto>() {
            @Override
            public void onResponse(Call<UsersResponseDto> call, Response<UsersResponseDto> response) {
                UsersResponseDto usersResponseDto = response.body();
                LinkedList<UserResponseDto> users = usersResponseDto.getUsers();
                ClientInfo.updateUserList(users);
                users.forEach(user ->  {
                    System.out.println(" username " + user.getUsername() + " online is " + user.getOnline() + " id " + user.getId());
                });


            }

            @Override
            public void onFailure(Call<UsersResponseDto> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        };
        asyncResponse.enqueue(callback);
    }
}

package ru.nsu.terekhov.dto;

import java.util.LinkedList;

public class MessagesResponseDto {
    private LinkedList<Message> messages;

    public MessagesResponseDto(LinkedList<Message> messages) {
        this.messages = messages;
    }

    public LinkedList<Message>  getMessages() {
        return messages;
    }
}

package ru.ccfit.terekhov.model.messages;

import static ru.ccfit.terekhov.other.Constatnts.DELIMETER_IN_PACKET;
import static ru.ccfit.terekhov.other.Constatnts.DISCONNECT_MSG;

public class DisconnectMessage implements Message {
    private final int type = DISCONNECT_MSG;



    @Override
    public String toString() {
        return type + DELIMETER_IN_PACKET;
    }

    @Override
    public byte[] toByteArray() {
        return toString ().getBytes ();
    }

    @Override
    public int getTypeOfMessage() {
        return type;
    }
}

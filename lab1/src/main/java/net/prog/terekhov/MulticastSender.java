/*
 * Developed by Terekhov Roman on 18.09.18 1:34.
 * Last modified 18.09.18 1:29.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class MulticastSender implements Runnable {
    private final Logger logger = Logger.getLogger (MulticastSender.class.getName ());
    private static final long DELAY_FOR_SENDER = 1000;
    private final String multicastGroup;
    private final int port;
    private DatagramSocket socket;
    private final UUID id;
    private final int bufferSize;


    MulticastSender(String multicastGroup, int port, int bufferSize) throws SocketException {
        this.multicastGroup = multicastGroup;
        this.port = port;
        socket = new DatagramSocket ();
        id = UUID.randomUUID ();
        this.bufferSize = bufferSize;
    }


    public void run() {
        InetAddress group;
        try {
            group = InetAddress.getByName (multicastGroup);
            byte[] buf = new byte[bufferSize];
            buf = id.toString ().getBytes ();
            DatagramPacket packet = new DatagramPacket (buf, buf.length, group, port);
            while (!Thread.currentThread ().isInterrupted ()) {
                socket.send (packet);
                //logger.info ("sent packet from id " + id);
                sleep (DELAY_FOR_SENDER);
            }
        } catch (InterruptedException | IOException e) {
            logger.log (Level.SEVERE, e.getMessage ());
        }
    }
}




package ru.nsu.terekhov.client.commands;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.dto.UserLoginRequestDto;
import ru.nsu.terekhov.dto.UserResponseDto;

import java.io.IOException;
import java.util.Scanner;

public class LoginCommand implements AbstractCommand {
    private ApiServiceInterface api = ApiServiceInterface.newInstance();
    private Scanner input = new Scanner(System.in);

    public LoginCommand() {
    }

    @Override
    public void execute() throws IOException {
        if (ClientInfo.isOnline()) {
            System.out.println("Вы уже вошли в чат");
            return;
        }

        System.out.println("Введите свой никнейм: ");
        String username = input.nextLine();
        UserLoginRequestDto newUser = new UserLoginRequestDto(username);
        Call<User> asyncResponse = api.postUser(newUser);


        Callback<User> callback = new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                switch (response.code()) {
                    case 401: {
                        System.out.println("Username" + username + " has already taken, pls choose another");
                        break;
                    }
                    case 200: {
                        if (response.body() != null) {
                            ClientInfo.setUserInfo(response.body());
                            ClientInfo.addUserToUserList(new UserResponseDto(response.body().getId(), response.body().getUsername(), response.body().getOnline()));
                            System.out.println("Вы успешно вошли в чат, привет " + ClientInfo.getUsername());
                            break;
                        } else {
                            System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                            break;
                        }
                    }
                    default: {
                        System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable throwable) {
                System.out.println("Ошибка при отправке запроса на сервер :" + throwable.getMessage());
            }
        };

        asyncResponse.enqueue(callback);
    }
}

package ru.nsu.terekhov.client.commands;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.UserLogoutResponseDto;

public class LogoutCommand implements AbstractCommand {
    private ApiServiceInterface api = ApiServiceInterface.newInstance();

    @Override
    public void execute() {
        if (!ClientInfo.isOnline()) {
            System.out.println("Вы не вошли в чат , чтобы из него выходить");
            return;
        }
        Call<UserLogoutResponseDto> asyncResponse = api.postUserLogout("Token " + ClientInfo.getToken());

        Callback<UserLogoutResponseDto> callback = new Callback<UserLogoutResponseDto>() {
            @Override
            public void onResponse(Call<UserLogoutResponseDto> call, Response<UserLogoutResponseDto> response) {
                switch (response.code()) {
                    case 403: {

                        break;
                    }
                    case 200: {
                        System.out.println("Вы вышли из чата, можете залогиться под старым именем если он будет свободно");
                        ClientInfo.clearUserInfo();
                        break;
                    }
                    default: {
                        System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserLogoutResponseDto> call, Throwable throwable) {
                System.out.println("Ошибка при отправке запроса на сервер :" + throwable.getMessage());
            }
        };

        asyncResponse.enqueue(callback);

    }
}

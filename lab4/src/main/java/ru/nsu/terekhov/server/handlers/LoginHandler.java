package ru.nsu.terekhov.server.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Authenticator;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.dto.UserLoginRequestDto;
import ru.nsu.terekhov.common.ExchangeClass;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import static ru.nsu.terekhov.responses.HttpStatusCodes.StatusCodes.*;

@SuppressWarnings("Duplicates")
public class LoginHandler implements HttpHandler, AbstractHandler {
    private static final Logger logger = Logger.getLogger(LoginHandler.class.getName());
    private ConcurrentHashMap<String, User> users;
    private AtomicInteger currentUserId = new AtomicInteger(0);

    public LoginHandler(ConcurrentHashMap<String, User> users) {
        this.users = users;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        logger.info("trying to login to server from" + exchange.getRemoteAddress());
        ExchangeClass exchangeClass = validateRequest(exchange);
        Gson gson = new Gson();
        boolean ok = exchangeClass.isOk();
        if (!ok) {
            writeResponse(exchange, "");
            return;
        }

        Reader bodyReader = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
        UserLoginRequestDto userLoginRequestDto = gson.fromJson(bodyReader, UserLoginRequestDto.class);
        String username = userLoginRequestDto.getUsername();

        if (username == null) {
            exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
            writeResponse(exchange, "");
            return;
        }

        if (users.containsKey(username)) {
            logger.warning(MessageFormat.format("Username {0} has already taken", username));
            addHeaderToResponse(exchange,"WWW-Authenticate", "Token realm='Username is already in use'");
            exchange.sendResponseHeaders(UNAUTHORIZED.getCode(), 0);
            writeResponse(exchange, "");

        } else {
            User newUser = new User(currentUserId.getAndIncrement(), username, true, UUID.randomUUID().toString());
            logger.info("new user with username " + username);
            addHeaderToResponse(exchange,"Content-type", "application/json");
            String json = gson.toJson(newUser);
            exchange.sendResponseHeaders(OK.getCode(), json.length());
            writeResponse(exchange, json);
            users.put(username, newUser);
        }


    }


    @SuppressWarnings("Duplicates")
    @Override
    public ExchangeClass validateRequest(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        if (!requestMethod.equals("POST")) {
            exchange.sendResponseHeaders(METHOD_NOT_ALLOWED.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        //Authenticator
        Headers requestHeaders = exchange.getRequestHeaders();
        if (!requestHeaders.containsKey("Content-type")) {
            exchange.sendResponseHeaders(NOT_ACCEPTABLE.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        List<String> contentTypeStrings = requestHeaders.get("Content-type");
        if (!contentTypeStrings.contains("application/json") || contentTypeStrings.size() != 1) {
            exchange.sendResponseHeaders(NOT_ACCEPTABLE.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        return new ExchangeClass(exchange, true, null);
    }
}

package ru.nsu.terekhov.dto;

import java.util.LinkedList;

public class UsersResponseDto {
    private LinkedList<UserResponseDto> users;

    public UsersResponseDto(LinkedList<UserResponseDto> users) {
        this.users = users;
    }

    public LinkedList<UserResponseDto> getUsers() {
        return users;
    }
}

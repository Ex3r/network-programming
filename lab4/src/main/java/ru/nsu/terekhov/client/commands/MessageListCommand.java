package ru.nsu.terekhov.client.commands;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.Message;
import ru.nsu.terekhov.dto.MessagesResponseDto;
import ru.nsu.terekhov.dto.UserResponseDto;
import ru.nsu.terekhov.dto.UsersResponseDto;

import java.io.IOException;
import java.util.LinkedList;
import java.util.TimerTask;

public class MessageListCommand extends TimerTask {
    private ApiServiceInterface api = ApiServiceInterface.newInstance();



    @Override
    public void run() {
        if (ClientInfo.getToken().isEmpty()) {
            return;
        }

        Call<MessagesResponseDto> asyncResponse = api.getMessages("Token " + ClientInfo.getToken(), ClientInfo.getCurrentAmountOfMessages(), 100);

        Callback<MessagesResponseDto> callback = new Callback<MessagesResponseDto>() {
            @Override
            public void onResponse(Call<MessagesResponseDto> call, Response<MessagesResponseDto> response) {
                switch (response.code()) {
                    case 403: {

                        break;
                    }
                    case 200: {
                        MessagesResponseDto messages = response.body();
                        if (response.body()!=null) {
                            LinkedList<Message> messagesList = messages.getMessages();
                            messagesList.forEach(message -> {
                                if (!ClientInfo.containsUserId(message.getAuthor())) {
                                    Call<UsersResponseDto> callUsers = api.getUsers("Token " + ClientInfo.getToken());
                                    try {
                                        UsersResponseDto users = callUsers.execute().body();
                                        LinkedList<UserResponseDto> usersList = users.getUsers();
                                        ClientInfo.updateUserList(usersList);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            ClientInfo.addGottedMessages(messagesList);
                        }

                        break;
                    }
                    default: {
                        System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<MessagesResponseDto> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        };

        asyncResponse.enqueue(callback);
    }
}

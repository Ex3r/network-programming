package ru.ccfit.terekhov.model.messages;

import static ru.ccfit.terekhov.other.Constatnts.DELIMETER_IN_PACKET;
import static ru.ccfit.terekhov.other.Constatnts.HELLO_MSG;

public class HelloMessage implements Message {
    private final int type = HELLO_MSG;
    private String uuid;


    public HelloMessage(String uuid) {
        this.uuid = uuid;
    }


    @Override
    public String toString() {
        return type + DELIMETER_IN_PACKET +uuid;
    }

    @Override
    public byte[] toByteArray() {
        return toString ().getBytes ();
    }

    @Override
    public int getTypeOfMessage() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }
}

package ru.nsu.terekhov.client;

import ru.nsu.terekhov.client.commands.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;

public class InputReader {
    private CommandsHolder commands = new CommandsHolder();
    private MessageCommand messageCommand = new MessageCommand();

    public InputReader() {
        commands.addCommand("login", new LoginCommand());
        commands.addCommand("logout", new LogoutCommand());
        commands.addCommand("users", new UsersCommand());
    }

    public void start() throws IOException {
        printUsage();
        Timer messageListGetter = new Timer("Message list getter");
        messageListGetter.schedule(new MessageListCommand(), 1000L, 1000L);

        Timer usersInfoGetter = new Timer("usersInfoGetter  getter");
        usersInfoGetter.schedule(new UsersLoginLogoutInfoCommand(), 1000L, 1000L);

        Scanner input = new Scanner(System.in);
        String inputString;
        while (!Thread.currentThread().isInterrupted()) {
            inputString = input.nextLine();
            boolean isCommand = checkIsCommand(inputString);
            if (isCommand) {
                if (commands.getCommands().containsKey(inputString.substring(1))) {
                    commands.getCommands().get(inputString.substring(1)).execute();
                } else {
                    System.out.println("UNKNOWN COMMAND");
                }
            } else {
                messageCommand.execute(inputString);
            }
        }
        messageListGetter.cancel();
        usersInfoGetter.cancel();
    }

    private boolean checkIsCommand(String maybeCommand) {
        if (!maybeCommand.isEmpty()) {
            return maybeCommand.charAt(0) == '/';
        } else {
            return false;
        }
    }

    private void printUsage() {
        String description = "использование чата:\n /login - для того, чтобы авторизоваться в чате\n" +
                "/users -посмотреть список пользователей\n /logout - для того, чтобы выйти из чата\n";
        System.out.println(description);
    }
}

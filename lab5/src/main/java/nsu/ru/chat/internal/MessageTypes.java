package nsu.ru.chat.internal;

public class MessageTypes {
    public static final String USUAL = "usual";
    public static final String ERROR = "error";
    public static final String GREET_USER = "greet_user";
    public static final String NOTIFY_ABOUT_NEW = "notify_about_new";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";
    public static final String USERS = "/users";
}

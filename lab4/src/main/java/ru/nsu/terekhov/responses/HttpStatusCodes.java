package ru.nsu.terekhov.responses;

public class HttpStatusCodes {
    public static  enum StatusCodes {
        METHOD_NOT_ALLOWED(405),
        NOT_ACCEPTABLE(406),
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        FORBIDDEN(403),
        NOT_FOUND(404),
        OK(200);
        private final int code;

        StatusCodes(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}

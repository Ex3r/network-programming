import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashMap;
import java.util.Iterator;
import org.xbill.DNS.*;


public class Proxy implements Runnable {
    int bufferSize = 8192;
    int port;
    String host;
    DatagramChannel dns; // Channel for send and receive DNS messages (async)
    String dnsServer = ResolverConfig.getCurrentConfig().server();
    HashMap<Integer, SelectionKey> dnsMap = new HashMap<Integer, SelectionKey>(); // Connection by Message ID

    static final byte[] NO_AUTH = new byte[] { 0x05, 0x00 };
    static final byte CONNECTION_ESTABLISHED = 0x00;
    static final byte GENERAL_FAILURE = 0x01;
    static final byte CONNECTION_REFUSED = 0x05;
    static final byte PROTOCOL_ERROR = 0x07;
    static final byte UNSUPPORTED_ADDRESS_TYPE = 0x08;
    static final byte SOCKS_VERSION = 0x05;
    static final byte IPv4 = 0x01;
    static final byte DOMAIN = 0x03;
    static final byte CONNECT = 0x01;

    static class Attachment {
        ByteBuffer in;
        ByteBuffer out;
        SelectionKey peer;
        InetAddress address;
        int p; // Connection port
        boolean saydHello = false; // true after handshake

        public Attachment(int bufferSize) {
            in = ByteBuffer.allocate(bufferSize);
            out = ByteBuffer.allocate(bufferSize);
        }
    }

    public static void main(String[] args) {
        Proxy server = new Proxy();
        server.host = "127.0.0.1";
        server.port = 1080;
        server.run();
    }

    @Override
    public void run() {
        try {
            Selector selector = SelectorProvider.provider().openSelector();
            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.socket().bind(new InetSocketAddress(host, port));
            serverChannel.register(selector, serverChannel.validOps());

            dns = DatagramChannel.open();
            dns.configureBlocking(false);
            dns.connect(new InetSocketAddress(dnsServer, 53));
            SelectionKey dnsKey = dns.register(selector, SelectionKey.OP_READ);

            while (selector.select() > -1) {
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (key.isValid()) {
                        try {
                            if (key == dnsKey && key.isReadable()) {
                                receiveDns();
                            } else if (key.isAcceptable()) {
                                System.out.println("Connection accepted");
                                accept(key);
                            } else if (key.isConnectable()) {
                                System.out.println("Connecting sockets");
                                connect(key);
                            } else if (key.isReadable()) {
                                System.out.println("Read to socket");
                                read(key);
                            } else if (key.isWritable()) {
                                System.out.println("Write from socket");
                                write(key);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            close(key);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Something wrong");
            System.exit(0);
        }
    }

    private void accept(SelectionKey key) {
        try {
            SocketChannel newChannel = ((ServerSocketChannel) key.channel()).accept();
            newChannel.configureBlocking(false);
            newChannel.register(key.selector(), SelectionKey.OP_READ);
        } catch (IOException e) {
            System.out.println("Can`t create new channel");
            System.exit(0);
        }
    }

    private void read(SelectionKey key) {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        if (attachment == null) {
            key.attach(attachment = new Attachment(bufferSize));
        }
        try {
            if (channel.read(attachment.in) < 1) {
                close(key);
            } else if (attachment.peer == null) {
                System.out.println("(authorize)");
                authorize(key, attachment);
            } else {
                System.out.println("(read data)");
                attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_WRITE);
                key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
                attachment.in.flip();
            }
        } catch (IOException e) {
            System.out.println("Connection refused");
            attachment.in.put(genAnswer(key, CONNECTION_REFUSED)).flip();
        }
    }

    private void write(SelectionKey key) {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        try {
            if (channel.write(attachment.out) == -1) {
                close(key);
            } else if (attachment.out.remaining() == 0) {
                attachment.out.clear();
                key.interestOps(SelectionKey.OP_READ);
                if (attachment.peer != null) {
                    attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_READ);
                }
            }
        } catch (IOException e) {
            System.out.println("Connection closed");
            attachment.out.put(genAnswer(key, CONNECTION_REFUSED)).flip();
        }
    }

    private void connect(SelectionKey key) {
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        try {
            channel.finishConnect();
        } catch (Exception ex) {
            System.out.println("Connection refused");
            attachment.out.put(genAnswer(key, CONNECTION_REFUSED)).flip();
            return;
        }
        attachment.in.put(genAnswer(key, CONNECTION_ESTABLISHED)).flip();
        attachment.out = ((Attachment) attachment.peer.attachment()).in;
        ((Attachment) attachment.peer.attachment()).out = attachment.in;
        attachment.peer.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
        key.interestOps(0);
    }

    private void close(SelectionKey key) {
        key.cancel();
        try {
            key.channel().close();
        } catch (IOException e) {
            System.out.println("Can`t close channel");
            return;
        }
        SelectionKey peerKey = ((Attachment) key.attachment()).peer;
        if (peerKey != null) {
            ((Attachment)peerKey.attachment()).peer = null;
            if((peerKey.interestOps() & SelectionKey.OP_WRITE) == 0) {
                ((Attachment)peerKey.attachment()).out.flip();
            }
            peerKey.interestOps(SelectionKey.OP_WRITE);
        }
    }

    private void receiveDns() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int len = dns.read(buffer);
        if (len <= 0) {
            return;
        }
        Message msg = new Message(buffer.array());
        Record[] recs = msg.getSectionArray(1);
        for (Record rec : recs) {
            if (rec instanceof ARecord) {
                ARecord arec = (ARecord) rec;
                int id = msg.getHeader().getID();
                SelectionKey key = dnsMap.get(id);
                if (key == null) {
                    continue;
                }
                Attachment attachment = (Attachment) key.attachment();
                attachment.address = arec.getAddress();
                System.out.println("Proxy to : " + arec.getAddress() + " " + attachment.p);
                createPeer(key);
                return;
            }
        }
        System.out.println("No A type record in DNS answer, I don`t play so");
    }

    private void authorize(SelectionKey key, Attachment attachment) {
        byte[] ar = attachment.in.array();
        // Handshake
        if (!attachment.saydHello) {
            if (ar[0] != SOCKS_VERSION || ar[1] == 0x00) {
                System.out.println("Wrong method or protocol version");
                attachment.out.put(genAnswer(key, PROTOCOL_ERROR)).flip();
                key.interestOps(SelectionKey.OP_WRITE);
                return;
            }
            for (int i = 2; i <= 2 + ar[1]; i++) {
                if (ar[i] == 0) {
                    attachment.out.put(NO_AUTH).flip();
                    attachment.in.clear();
                    key.interestOps(SelectionKey.OP_WRITE);
                    attachment.saydHello = true;
                    return;
                }
            }
        }
        // Authorization
        if (ar[ 0 ] != SOCKS_VERSION && ar[1] != CONNECT || attachment.in.position() < 9) {
            attachment.out.put(genAnswer(key, PROTOCOL_ERROR)).flip();
            key.interestOps(SelectionKey.OP_WRITE);
        } else {
            int p = (((0xFF & ar[attachment.in.position()-2]) << 8) + (0xFF & ar[attachment.in.position()-1]));
            attachment.p = p;
            if (ar[3] == IPv4) {
                byte[] addr = new byte[] { ar[4], ar[5], ar[6], ar[7] };
                try {
                    InetAddress address = InetAddress.getByAddress(addr);
                    attachment.address = address;
                    createPeer(key);
                    System.out.println("Proxy to : " + address.getHostName() + " " + p);
                } catch (UnknownHostException e) {
                    attachment.out.put(genAnswer(key, PROTOCOL_ERROR)).flip();
                    key.interestOps(SelectionKey.OP_WRITE);
                    return;
                }
            } else if (ar[3] == DOMAIN) {
                int l = ar[4];
                char[] addr = new char[l];
                for (int i = 0; i < l; i++) {
                    addr[i] = (char)ar[i+5];
                }

                String domain = String.valueOf(addr) + ".";
                try {
                    Name name = Name.fromString(domain);
                    Record rec = Record.newRecord(name, Type.A, DClass.IN);
                    Message msg = Message.newQuery(rec);
                    dns.write(ByteBuffer.wrap(msg.toWire()));
                    dnsMap.put(msg.getHeader().getID(), key);
                } catch (IOException e) {
                    attachment.out.put(genAnswer(key, GENERAL_FAILURE)).flip();
                    key.interestOps(SelectionKey.OP_WRITE);
                    return;
                }
            } else {
                attachment.out.put(genAnswer(key, UNSUPPORTED_ADDRESS_TYPE)).flip();
                key.interestOps(SelectionKey.OP_WRITE);
                return;
            }
            attachment.in.clear();
        }
    }

    private void createPeer(SelectionKey key) {
        try {
            Attachment attachment = (Attachment) key.attachment();
            SocketChannel peer = SocketChannel.open();
            peer.configureBlocking(false);
            System.out.println("Proxy to : " + attachment.address.getHostName() + " " + attachment.p);

            peer.connect(new InetSocketAddress(attachment.address, attachment.p));
            SelectionKey peerKey = peer.register(key.selector(), SelectionKey.OP_CONNECT);
            key.interestOps(0);

            attachment.peer = peerKey;
            Attachment peerAttachement = new Attachment(bufferSize);
            peerAttachement.peer = key;
            peerKey.attach(peerAttachement);
        }
        catch (IOException e) {
            System.out.println("Can`t create peer");
            ((Attachment) key.attachment()).out.put(genAnswer(key, GENERAL_FAILURE)).flip();
        }
    }

    // Answer to server in SOCKS style
    private byte[] genAnswer(SelectionKey key, byte status) {
        SocketChannel channel = ((SocketChannel) key.channel());
        byte[] answer = new byte[10];
        answer[0] = SOCKS_VERSION;
        answer[1] = status;
        answer[3] = IPv4;
        String addr = "127.0.0.1";
        int p = 80;
        try {
            addr = ((InetSocketAddress) channel.getLocalAddress()).getHostName();
            p = ((InetSocketAddress) channel.getLocalAddress()).getPort();
        } catch (IOException e) {
            System.out.println("Can`t get socket address");
            answer[1] = GENERAL_FAILURE;
        }
        int i = 4;
        for (String s1 : addr.split("\\.")) {
            answer[i] = (byte)(Integer.parseInt(s1) - 128);
            i++;
        }
        answer[i] = (byte)(p >> 8);
        answer[i+1] = (byte)(p & 0xFF);
        return answer;
    }
}

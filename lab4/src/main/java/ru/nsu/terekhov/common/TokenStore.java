package ru.nsu.terekhov.common;

import ru.nsu.terekhov.dto.User;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class TokenStore {
    public static User findUserByToken(String token, ConcurrentHashMap<String, User> users) {
        User user = null;
        Collection<User> values = users.values();
        for (User u : values) {
            String userToken = u.getToken();
            if (userToken.equals(token)) {
                user = u;
                return user;
            }
        }

        return user;
    }

    public static  User findUserById(int id ,  ConcurrentHashMap<String, User> users) {
        User user = null;
        Collection<User> values = users.values();
        for (User u : values) {
            int userId = u.getId();
            if (userId == id) {
                user = u;
                return user;
            }
        }
        return user;
    }
}

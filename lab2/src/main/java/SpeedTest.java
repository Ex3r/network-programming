import java.time.ZonedDateTime;

public class SpeedTest {
    private final static long TICK = 1;
    private long currentRead = 0;
    private long totalRead = 0;
    private long startTime = 0;
    private long currentTime = 0;
    private long fileSize;
    private String id;


    public void start(long fileSize, String id) {
        startTime = ZonedDateTime.now ().toEpochSecond ();
        currentTime = startTime;
        this.fileSize = fileSize;
        this.id = id;

    }

    public void check(int read) {
        long timeNow = ZonedDateTime.now ().toEpochSecond ();
        currentRead += read;
        if (timeNow - currentTime >= TICK) {
            totalRead += currentRead;
            printResult ();
            currentTime = ZonedDateTime.now ().toEpochSecond ();
            currentRead = 0;
        }
    }

    private void printResult() {
        long momentumSpeed = currentRead / TICK / 1024;
        long averageSpeed;
        if ((currentTime - startTime) == 0) {
            averageSpeed = momentumSpeed;
        } else {
            averageSpeed = totalRead / ((currentTime - startTime)) / 1024;
        }

        StringBuilder output = new StringBuilder ().append (String.format ("uuid: %s\n", id)).append (String.format ("Received: %d%s \n", ((totalRead * 100) / fileSize), "%")).append (String.format ("Momentum speed: %d KB/Sec\n", momentumSpeed)).append (String.format ("Average speed: %d KB/Sec\n", averageSpeed));
        System.out.println (output);
    }

    public void finish() {
        System.out.println ("Client " + id + " finished successfully");
        totalRead += currentRead;
        printResult ();
    }
}

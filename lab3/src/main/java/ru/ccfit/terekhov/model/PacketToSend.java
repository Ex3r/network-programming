package ru.ccfit.terekhov.model;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class PacketToSend {
    private DatagramPacket packet;
    private int TTL = 100;

    public PacketToSend(DatagramPacket packet) {
        this.packet = packet;
    }


    public DatagramPacket getPacket() {
        return packet;
    }

    public int getTTL() {
        return TTL;
    }

    public void decrementTTL() {
        TTL--;
    }
}

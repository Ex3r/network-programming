package nsu.ru.chat.client.commands;

public interface AbstractCommand {
    void execute();
}

package ru.nsu.terekhov.server.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;
import ru.nsu.terekhov.dto.User;
import ru.nsu.terekhov.common.ExchangeClass;
import ru.nsu.terekhov.dto.UserResponseDto;
import ru.nsu.terekhov.dto.UsersResponseDto;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import static ru.nsu.terekhov.common.TokenStore.findUserById;
import static ru.nsu.terekhov.common.TokenStore.findUserByToken;
import static ru.nsu.terekhov.responses.HttpStatusCodes.StatusCodes.*;

public class UsersHandler implements HttpHandler, AbstractHandler {
    private static final Logger logger = Logger.getLogger(UsersHandler.class.getName());
    private ConcurrentHashMap<String, User> users;

    public UsersHandler(ConcurrentHashMap<String, User> users) {
        this.users = users;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        ExchangeClass exchangeClass = validateRequest(exchange);
        Gson gson = new Gson();
        boolean ok = exchangeClass.isOk();
        if (!ok) {
            writeResponse(exchange, "");
            return;
        }

        URI requestURI = exchange.getRequestURI();
        String uriStr = requestURI.toString();

        //проверяем выводить всех или только одного
        if (uriStr.lastIndexOf('/') == 0) {
            //выводим всех
            LinkedList<UserResponseDto> usersResponse = new LinkedList<>();
            users.values().forEach(u -> usersResponse.add(convertUserToUserResponseDto(u)));

            //200
            addHeaderToResponse(exchange, "Content-type", "application/json");
            String json = gson.toJson(new UsersResponseDto(usersResponse));
            exchange.sendResponseHeaders(OK.getCode(), json.length());
            writeResponse(exchange, json);
            return;
        }

        if (uriStr.lastIndexOf('/') == 6) {
            String userId = uriStr.substring(7);
            try {
                int id = Integer.parseInt(userId);
                User user = findUserById(id, users);
                if (user == null) {
                    //403
                    exchange.sendResponseHeaders(NOT_FOUND.getCode(), 0);
                    writeResponse(exchange, "");

                } else {
                    //200
                    addHeaderToResponse(exchange, "Content-type", "application/json");
                    String json = gson.toJson(convertUserToUserResponseDto(user));
                    exchange.sendResponseHeaders(OK.getCode(), json.length());
                    writeResponse(exchange, json);
                }
            } catch (IllegalArgumentException ex) {
                //400
                exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
                writeResponse(exchange, "");

            }
        } else {
            //400
            exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
            writeResponse(exchange, "");
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ExchangeClass validateRequest(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        if (!requestMethod.equals("GET")) {
            exchange.sendResponseHeaders(METHOD_NOT_ALLOWED.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }
        return new ExchangeClass(exchange, true, null);
    }

    private UserResponseDto convertUserToUserResponseDto(User user) {
        return new UserResponseDto(user.getId(), user.getUsername(), user.getOnline());
    }
}

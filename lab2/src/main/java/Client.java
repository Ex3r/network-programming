import org.apache.commons.cli.CommandLine;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class Client {
    private static final Logger logger = Logger.getLogger (Client.class.getName ());
    private static final int BUFFER_SIZE = 1024;
    private Socket socket;
    private UUID id;
    private String path;
    private String ip;
    private int port;


    public Client(String ip, int port, String path) {
        id = UUID.randomUUID ();
        this.path = path;
        this.ip = ip;
        this.port = port;
    }

    private boolean checkFileExists(String path) {
        Path filePath = Paths.get (path);
        return Files.exists (filePath);
    }

    public void startSend() {
        try {
            socket = new Socket (ip, port);
        } catch (IOException e) {
            logger.severe (e.getMessage ());
            return;
        }
        logger.info ("Client " + id + " has been started send file");
        byte[] buffer = new byte[Utilits.BUFFER_SIZE];
        File file;
        try {
            if (checkFileExists (path)) {
                logger.info ("File " + path + " already exist");
            }

            file = new File (path);
            FileInputStream fin = new FileInputStream (file);
            Long fileSize = file.length ();

            DataOutputStream outputStream = new DataOutputStream (socket.getOutputStream ());
            DataInputStream inputStream = new DataInputStream (socket.getInputStream ());

            outputStream.writeUTF (id.toString ());  //36 байт
            outputStream.writeUTF (path);  //само имя файла
            outputStream.writeLong (fileSize);

            // -1 read (если стрим закончился)
            long byteRead = 0;
            while (byteRead < fileSize) {
                long diff = fileSize - byteRead;
                if (diff >= BUFFER_SIZE) {
                    int read = fin.read (buffer, 0, BUFFER_SIZE);
                    outputStream.write (buffer, 0, read);
                    byteRead += read;
                } else {
                    int readed = fin.read (buffer, 0, (int) diff);
                    outputStream.write (buffer, 0, readed);
                    byteRead += readed;
                }
            }


            String answer = inputStream.readUTF ();
            System.out.println (answer);


            socket.close ();
            inputStream.close ();
            outputStream.close ();


        } catch (SocketException e) {
            logger.severe ("Сервер упал: " + e.getMessage ());
        } catch (IOException e) {
            e.printStackTrace ();
        }

    }

    public static void main(String[] args) {
        Map<String, String> parametersForOptions = new LinkedHashMap<> ();
        parametersForOptions.put (Utilits.IP, "ip address");
        parametersForOptions.put (Utilits.PORT, "port");
        parametersForOptions.put (Utilits.PATH, "path for file which will be downloading");
        CommandLineParserCustom commandParser = new CommandLineParserCustom (parametersForOptions);
        CommandLine commandParserArgs = commandParser.getArgs (args);
        if (commandParserArgs == null) {
            return;
        }

        String ip = commandParserArgs.getOptionValue (Utilits.IP);
        String portStr = commandParserArgs.getOptionValue (Utilits.PORT);
        String path = commandParserArgs.getOptionValue (Utilits.PATH);

        int port = Integer.parseInt (portStr);
        Client client = new Client (ip, port, path);
        client.startSend ();
    }
}

package ru.nsu.terekhov.server.handlers;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import ru.nsu.terekhov.common.ExchangeClass;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

public interface AbstractHandler {
    ExchangeClass validateRequest(HttpExchange exchange) throws IOException;
    default void writeResponse(HttpExchange httpResponse, String response) throws IOException {
        OutputStream os = httpResponse.getResponseBody();
        os.write(response.getBytes(Charset.forName("UTF-8")));
        os.close();
    }
    default void addHeaderToResponse(HttpExchange exchange, String keyHeader, String valueHeader) {
        Headers responseHeaders = exchange.getResponseHeaders();
        responseHeaders.add(keyHeader,valueHeader);
    }
}

package nsu.ru.chat.client.commands;


import nsu.ru.chat.client.ClientEndpoint;
import nsu.ru.chat.client.ClientInfo;
import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.DeploymentException;
import javax.websocket.Session;
import java.io.IOException;
import java.net.URI;
import java.util.Scanner;
import java.util.logging.Logger;

import static nsu.ru.chat.config.SharedConstants.SERVER;

public class LoginCommand implements AbstractCommand {
    private static final Logger log = Logger.getLogger(LoginCommand.class.getName());
    private Scanner input = new Scanner(System.in);
    private ClientManager client;

    public LoginCommand(ClientManager client) {
        this.client = client;
    }

    @Override
    public void execute() {
        if (ClientInfo.isLogined()) {
            System.out.println("Вы уже вошли в чат");
            return;
        }

        System.out.println("Введите свой никнейм: ");
        String username = input.nextLine();

        URI uri = URI.create(SERVER + "/" + username + "/");
        Session session = null;
        try {
            session = client.connectToServer(ClientEndpoint.class, uri);
        } catch (DeploymentException | IOException e) {
            log.severe("Ошибка подключения к серверу, причина: " + e.getMessage());
        }
        if (session == null) {
            log.severe("Ошибка подключения к серверу");
        }
        if (session != null && session.isOpen()) {
            ClientInfo.setSession(session);
            ClientInfo.setLogined(true);
            ClientInfo.setUserName(username);
        }


    }
}

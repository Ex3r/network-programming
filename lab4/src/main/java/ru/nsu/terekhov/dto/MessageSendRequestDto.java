package ru.nsu.terekhov.dto;

public class MessageSendRequestDto {
    private String message;

    public MessageSendRequestDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

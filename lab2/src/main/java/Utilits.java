import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class Utilits {
    private static final Logger logger = Logger.getLogger (Utilits.class.getName ());
    public static final int BUFFER_SIZE = 1024;
    public static final String IP = "ip";
    public static final String PORT = "port";
    public static final String PATH = "path";

    public static void createFolder(String folderPath) throws IOException {
        Path dirPath = Paths.get (folderPath);
        boolean exists = Files.exists (dirPath);
        if (exists) {
            logger.info ("Directory " + folderPath + " already exists!");
        } else {
            try {
                Files.createDirectories (dirPath);
                logger.info ("Directory " + folderPath + " has been created");
            } catch (IOException e) {
                logger.severe (e.getMessage ());
                throw e;
            }
        }
    }
}

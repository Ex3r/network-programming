package ru.nsu.terekhov.client.commands;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.terekhov.client.ClientInfo;
import ru.nsu.terekhov.client.api.ApiServiceInterface;
import ru.nsu.terekhov.dto.UserResponseDto;
import ru.nsu.terekhov.dto.UsersResponseDto;

import java.util.LinkedList;
import java.util.TimerTask;

import static java.text.MessageFormat.format;

public class UsersLoginLogoutInfoCommand extends TimerTask {
    private ApiServiceInterface api = ApiServiceInterface.newInstance();

    @Override
    public void run() {
        if (ClientInfo.getToken().isEmpty()) {
            return;
        }

        Call<UsersResponseDto> asyncResponse = api.getUsers("Token " + ClientInfo.getToken());

        Callback<UsersResponseDto> callback = new Callback<UsersResponseDto>() {
            @Override
            public void onResponse(Call<UsersResponseDto> call, Response<UsersResponseDto> response) {
                switch (response.code()) {
                    case 403: {

                        break;
                    }
                    case 200: {
                        UsersResponseDto usersResponseDto = response.body();
                        LinkedList<UserResponseDto> newUsers = usersResponseDto.getUsers();
                        LinkedList<UserResponseDto> tempList = new LinkedList<>();
                        if (newUsers != null) {
                            tempList.addAll(newUsers);
                        }
                        LinkedList<UserResponseDto> oldUsers = ClientInfo.getUserList();
                        tempList.removeAll(oldUsers);
                        tempList.forEach(user -> {
                            if (!user.getUsername().equals(ClientInfo.getUsername())) {
                                System.out.println(format("New user {0} joined to chat", user.getUsername()));
                            }
                        });

                        oldUsers.removeAll(newUsers);
                        oldUsers.forEach(user -> {
                            if (!user.getUsername().equals(ClientInfo.getUsername())) {
                                System.out.println(format("User {0} leaved the channel", user.getUsername()));
                            }

                        });

                        ClientInfo.clearUserList();
                        newUsers.forEach(ClientInfo::addUserToUserList);
                        break;
                    }
                    default: {
                        System.out.println("Неизвестный ответ от сервера , код ответа: " + response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<UsersResponseDto> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        };

        asyncResponse.enqueue(callback);
    }
}

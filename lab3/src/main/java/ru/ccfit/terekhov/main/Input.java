package ru.ccfit.terekhov.main;

import javafx.util.Pair;
import ru.ccfit.terekhov.model.messages.UsualMessage;

import java.net.InetAddress;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class Input implements Runnable {
    private static final Logger logger = Logger.getLogger (Input.class.getName ());
    private Sender sender;
    private Set<Pair<InetAddress, Integer>> neighbours;

    public Input(Sender sender) {
        this.sender = sender;
        this.neighbours = sender.getNeighbours ();
    }


    @Override
    public void run() {
        Scanner scanner = new Scanner (System.in);
        try {
            while (!Thread.currentThread ().isInterrupted ()) {
                //logger.info ("waiting for keyboard input");
                int size = neighbours.size ();
                System.out.println ("Введите сообщение, которое отправится " + size + " соседям");
                if (size == 0) {
                    sleep (5000);
                } else {
                    String inputLine = scanner.nextLine ();
                    if ("exit".equals (inputLine)) {
                        Thread.currentThread ().interrupt ();
                        throw new InterruptedException ("Ввели слово exit, остановка программы");
                    } else {
                        neighbours.forEach ((neighbour) -> {
                            UsualMessage usualMessage = new UsualMessage (UUID.randomUUID ().toString (), inputLine);
                            sender.sendUsualMessage (usualMessage, new Pair<> (neighbour.getKey (), neighbour.getValue ()));
                        });
                    }
                }

            }
        } catch (InterruptedException e) {
            neighbours.forEach ((neighbour) -> {
                sender.sendDisconnectMessage (new Pair<> (neighbour.getKey (), neighbour.getValue ()));
            });
            logger.info (e.getMessage ());
        }
    }
}

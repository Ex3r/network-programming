/*
 * Developed by Terekhov Roman on 07.09.18 17:01.
 * Last modified 07.09.18 16:59.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoaderProperties {
    private String propFileName;

    LoaderProperties(String propFileName) {
        this.propFileName = propFileName;
    }

    public Properties getPropValues() throws IOException {
        InputStream inputStream;
        Properties properties = new Properties ();
        inputStream = getClass ().getClassLoader ().getResourceAsStream (propFileName);

        if (inputStream != null) {
            properties.load (inputStream);
        } else {
            throw new FileNotFoundException ("property file '" + propFileName + "' not found in the classpath");
        }
        return properties;
    }
}

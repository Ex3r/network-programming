package ru.nsu.terekhov.client.commands;

import java.io.IOException;

public interface AbstractCommand {
    void execute() throws IOException;
}

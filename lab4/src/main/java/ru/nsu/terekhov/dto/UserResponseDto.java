package ru.nsu.terekhov.dto;

import java.util.Objects;

public class UserResponseDto {
    private int id;
    private String username;
    private Boolean online;

    public UserResponseDto(int id, String username, Boolean online) {
        this.id = id;
        this.username = username;
        this.online = online;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getOnline() {
        return online;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponseDto that = (UserResponseDto) o;
        return id == that.id &&
                username.equals(that.username) &&
                online.equals(that.online);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, online);
    }
}

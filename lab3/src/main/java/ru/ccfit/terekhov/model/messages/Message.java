package ru.ccfit.terekhov.model.messages;

public interface Message {
    String toString();
    byte[] toByteArray();
    int getTypeOfMessage();
}

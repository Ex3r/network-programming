package nsu.ru.chat.client.commands;

import com.google.gson.Gson;
import nsu.ru.chat.client.ClientInfo;
import nsu.ru.chat.model.Message;

import javax.websocket.Session;
import java.io.IOException;
import java.util.logging.Logger;

import static nsu.ru.chat.internal.MessageTypes.USUAL;


public class MessageCommand {
    private static final Logger log = Logger.getLogger(MessageCommand.class.getName());
    private static final Gson gson = new Gson();

    public void execute(String message) {
        if (!ClientInfo.isLogined()) {
            System.out.println("Необходима авторизация для отправки сообщения, авторизуйтесь с помощью команды /login");
            return;
        }

        Session session = ClientInfo.getSession();
        Message msg = new Message(USUAL, ClientInfo.getUserName(), message);
        String json = gson.toJson(msg);
        try {
            session.getBasicRemote().sendText(json);
        } catch (IOException e) {
            log.severe("Ошибка отправки сообщения, причина: " + e.getMessage());
        }


    }
}

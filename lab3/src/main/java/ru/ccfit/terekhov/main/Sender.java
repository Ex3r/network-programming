package ru.ccfit.terekhov.main;

import javafx.util.Pair;
import ru.ccfit.terekhov.model.PacketToSend;
import ru.ccfit.terekhov.model.messages.*;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;


public class Sender implements Runnable {
    private static final Logger logger = Logger.getLogger (Sender.class.getName ());
    private Set<Pair<InetAddress, Integer>> neighbours;
    private Map<String, PacketToSend> taskStorage;
    private DatagramSocket socket;



    public Sender(DatagramSocket socket, Set<Pair<InetAddress, Integer>> neighbours, Map<String, PacketToSend> taskStorage ) {
        this.taskStorage = taskStorage;
        this.socket = socket;
        this.neighbours = neighbours;
    }

    public void sendHelloMessage (HelloMessage helloMessage, Pair<InetAddress, Integer> infoAboutParent) {
        byte[] byteHelloPacket = helloMessage.toString ().getBytes ();
        DatagramPacket packet = new DatagramPacket (byteHelloPacket, byteHelloPacket.length, infoAboutParent.getKey (), infoAboutParent.getValue ());
        putMessageInStorage (helloMessage.getUuid (), new PacketToSend (packet));


    }

    public void sendUsualMessage(UsualMessage msg, Pair<InetAddress, Integer> sendTo) {
        byte[] msgInBytes = msg.toByteArray ();
        DatagramPacket packet = new DatagramPacket (msgInBytes, msgInBytes.length, sendTo.getKey (), sendTo.getValue ());
        putMessageInStorage (msg.getUuid (), new PacketToSend (packet));
    }

    public void sendPingMessage(Pair<InetAddress, Integer> sendTo) {
        PingMessage ping = new PingMessage ();
        byte[] bytes = ping.toByteArray ();
        DatagramPacket packet = new DatagramPacket (bytes, bytes.length, sendTo.getKey (), sendTo.getValue ());
        try {
            send (packet);
        } catch (IOException e) {
            logger.severe (MessageFormat.format ("Ошибка при отправке сообщения{0}", e.getMessage ()));
        }
    }

    public void sendAck(Pair<InetAddress, Integer> sendTo, String uuid) throws IOException {
        AcknowledgeMessage ack = new AcknowledgeMessage (uuid);
        byte[] bytes = ack.toByteArray ();
        DatagramPacket packet = new DatagramPacket (bytes, bytes.length, sendTo.getKey (), sendTo.getValue ());
       // putMessageInStorage (ack.getUuid (), new PacketToSend (packet));
        send(packet);
    }

    public void sendDisconnectMessage(Pair<InetAddress, Integer> sendTo) {
        DisconnectMessage disconnectMessage = new DisconnectMessage ();
        byte[] bytes = disconnectMessage.toByteArray ();
        DatagramPacket packet = new DatagramPacket (bytes, bytes.length, sendTo.getKey (), sendTo.getValue ());
        try {
            send (packet);
        } catch (IOException e) {
            logger.severe (MessageFormat.format ("Ошибка при отправке сообщения{0}", e.getMessage ()));
        }
    }


    private void putMessageInStorage(String key, PacketToSend value) {
        taskStorage.put (key, value);
    }


    public void removeTask (String key) {
        taskStorage.remove (key);
    }


    private void send (DatagramPacket packet) throws IOException {
        socket.send (packet);
    }


    @Override
    public void run() {
        try {
            while (!Thread.currentThread ().isInterrupted ()) {

                sleep (1000);

                taskStorage.forEach ((id, packet) -> {
                    try {
                        send (packet.getPacket ());

                    } catch (IOException e) {
                        logger.severe ("Ошибка при отправке пакета");
                    }
                });
            }
        } catch (InterruptedException e) {
            e.printStackTrace ();
        }
    }


    public Map<String, PacketToSend> getTaskStorage() {
        return taskStorage;
    }

    public Set<Pair<InetAddress, Integer>> getNeighbours() {
        return neighbours;
    }
}

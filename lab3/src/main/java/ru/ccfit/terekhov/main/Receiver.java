package ru.ccfit.terekhov.main;

import javafx.util.Pair;
import ru.ccfit.terekhov.model.PacketToSend;
import ru.ccfit.terekhov.other.Constatnts;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.text.MessageFormat;
import java.util.*;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static ru.ccfit.terekhov.other.Constatnts.*;

public class Receiver implements Runnable {
    private static final Logger logger = Logger.getLogger (Receiver.class.getName ());
    private Sender sender;
    private PingService pingService;
    private int packetLossPercent;
    private DatagramSocket socket;
    private DatagramPacket packet;
    private Map<String, PacketToSend> taskStorage;
    private Set<Pair<InetAddress, Integer>> neighbours;
    private byte[] buffer = new byte[BUFFER_SIZE];

    public Receiver(DatagramSocket socket, int packetLossPercent, Sender sender, PingService pingService) {
        this.packetLossPercent = packetLossPercent;
        this.sender = sender;
        this.socket = socket;
        this.taskStorage = sender.getTaskStorage ();
        this.neighbours = sender.getNeighbours ();
        this.pingService = pingService;



    }


    private void receive() throws IOException {
        int packetLossGenerated = GeneratePacketLossNumber.generate (0, 99);
        if (packetLossGenerated < packetLossPercent ) {
            return;
        }

        packet = new DatagramPacket (buffer, BUFFER_SIZE);
        socket.receive (packet);

        InetAddress hostAddress = packet.getAddress ();
        int port = packet.getPort ();
        byte[] data = packet.getData ();
        List<String> strings = new ArrayList<>();
        Collections.addAll(strings, decode (data));
        String s = strings.get (1);
        //для того, чтобы корректно обрезать мусор от строки
        String uuid = s.substring (0, 36);

        String message = "";
        for (String string: strings) {
            message = new StringBuilder ().append (message).append (string).append (" ").toString ();
        }
        //logger.info ("Received message " + message);
        //logger.info(hostAddress + ":" + port);


        String typeOfMessage = strings.get (0);

        if (typeOfMessage.equals (HELLO_MSG.toString ()) || typeOfMessage.equals (USUAL_MSG.toString ())) {
            String messageUUID = uuid;
            sender.sendAck (new Pair<InetAddress, Integer> (hostAddress, port), messageUUID);
            logger.info ("Отправили подтверждение для " + messageUUID);


            if (typeOfMessage.equals (HELLO_MSG.toString ())) {
                neighbours.add (new Pair<> (hostAddress, port));
                pingService.addNewNeighbour (new Pair<> (hostAddress, port));
                logger.info(MessageFormat.format ("Получили hello packet от {0}:{1} добавили в список соседей", hostAddress.getHostAddress (), port));
            } else {
                System.out.println (MessageFormat.format ("From {0}:{1} received message {2}", hostAddress.getHostAddress (), port, strings.get (2)));
            }
            //так не работает, почему ? да хз хотя должно....
            //removeTask (messageUUID);

        }

        if (typeOfMessage.equals (ACK_MSG.toString ())) {
            String messageUUID = uuid;
            logger.info ("Получили подтверждение для сообщения " + messageUUID + " размер хранилища " + taskStorage.size ());
            boolean removeResult = removeTask (messageUUID);
            logger.info (MessageFormat.format ("Удалено ли сообщение {0} {1}", messageUUID, removeResult));

        }

        if (typeOfMessage.equals (DISCONNECT_MSG.toString ())) {
            logger.info (MessageFormat.format ("Client {0}:{1}wanted to disconnect", hostAddress.getHostAddress (), port));
            neighbours.removeIf (setElement -> setElement.getKey ().getHostAddress ().equals (hostAddress.getHostAddress ()) && setElement.getValue ().equals (port));
            pingService.removeNode (new Pair<> (hostAddress, port));
        }

        if (typeOfMessage.equals (PING_MSG.toString ())) {
            //logger.info (MessageFormat.format ("Received ping from {0}:{1}", hostAddress.getHostAddress (), port));
            pingService.updateInfoAboutNode (new Pair<> (hostAddress, port));
        }



    }

    private boolean removeTask(String id) {
        return taskStorage.entrySet ().removeIf (entry -> entry.getKey ().equals (id));
    }

    @Override
    public void run() {
        while(!Thread.currentThread ().isInterrupted ()) {
            try {
                receive ();
            } catch (IOException e) {
                logger.severe ("ошибка получения пакета " + e.getMessage ());
            }
        }

    }


    private String[] decode(byte[] encodedArray) {
        return new String(encodedArray, UTF_8).split(Constatnts.DELIMETER_IN_PACKET);
    }
}

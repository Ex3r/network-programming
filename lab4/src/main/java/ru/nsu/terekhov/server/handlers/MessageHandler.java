package ru.nsu.terekhov.server.handlers;

import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpPrincipal;
import ru.nsu.terekhov.common.ExchangeClass;
import ru.nsu.terekhov.dto.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

import static ru.nsu.terekhov.common.TokenStore.findUserByToken;
import static ru.nsu.terekhov.responses.HttpStatusCodes.StatusCodes.*;

@SuppressWarnings("Duplicates")
public class MessageHandler implements HttpHandler, AbstractHandler {
    private static final Logger logger = Logger.getLogger(MessageHandler.class.getName());
    private ConcurrentHashMap<String, User> users;
    private ConcurrentLinkedQueue<Message> messages;
    private AtomicInteger currentMessageId = new AtomicInteger(0);

    public MessageHandler(ConcurrentHashMap<String, User> users, ConcurrentLinkedQueue<Message> messages) {
        this.users = users;
        this.messages = messages;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        URI requestURI = exchange.getRequestURI();
        String query = requestURI.getQuery();
        if (query == null) {
            sendMessage(exchange);
        } else {
            getAllMessages(exchange);
        }


    }

    private void sendMessage(HttpExchange exchange) throws IOException {
        Gson gson = new Gson();

        ExchangeClass exchangeClass = checkRequestForSendingMessage(exchange);
        boolean ok = exchangeClass.isOk();
        if (!ok) {
            writeResponse(exchange, "");
            return;
        }

        HttpPrincipal principal = exchange.getPrincipal();
        String token = principal.getRealm();
        User user = findUserByToken(token, users);

        Reader bodyReader = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
        MessageSendRequestDto messageSendRequestDto = gson.fromJson(bodyReader, MessageSendRequestDto.class);
        String message = messageSendRequestDto.getMessage();
        if (message == null) {
            exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
            writeResponse(exchange, "");
            return;
        }

        MessageSendResponseDto responseDto = new MessageSendResponseDto(currentMessageId.getAndIncrement(), message);
        addHeaderToResponse(exchange, "Content-type", "application/json");
        String json = gson.toJson(responseDto);
        exchange.sendResponseHeaders(OK.getCode(), json.length());
        writeResponse(exchange, json);
        messages.add(new Message(responseDto.getId(), message, user.getId()));
    }


    //вывод всех сообщений
    private void getAllMessages(HttpExchange exchange) throws IOException {
        Gson gson = new Gson();

        ExchangeClass exchangeClass = checkRequestForGettingAllMessages(exchange);
        boolean ok = exchangeClass.isOk();
        if (!ok) {
            writeResponse(exchange, "");
            return;
        }
        AtomicInteger offset = new AtomicInteger(0);
        AtomicInteger count = new AtomicInteger(10);
        boolean correctParsing = parseQueries(exchange, offset, count);
        if (!correctParsing) {
            return;
        }


        HttpPrincipal principal = exchange.getPrincipal();
        String token = principal.getRealm();
        User user = findUserByToken(token, users);


        addHeaderToResponse(exchange, "Content-type", "application/json");
        List<Message> messageList = new ArrayList<>(messages);
        LinkedList<Message> responseList = new LinkedList<>();
        int size = messageList.size();
        if (offset.get() >= 0) {
            for (int i = offset.get(), tempCount = 0; (i < size) && (tempCount < count.get()); i++) {
                responseList.add(messageList.get(i));
            }
        } else {
            offset.getAndSet(-offset.get());
            if (offset.get() > size) {
                offset.getAndSet(size);
            }

            for (int i = size - offset.get(), tempCount = 0; (i < size) && (tempCount < count.get()); i++) {
                responseList.add(messageList.get(i));
            }

        }
        String json = gson.toJson(new MessagesResponseDto(responseList));
        exchange.sendResponseHeaders(OK.getCode(), json.length());
        writeResponse(exchange, json);
    }


    private boolean parseQueries(HttpExchange exchange, AtomicInteger offset, AtomicInteger count) throws IOException {
        String query = exchange.getRequestURI().getQuery();
        String[] queries = query.split("&");

        switch (queries.length) {
            case 0: {
                sendBadRequest(exchange);
                return false;
            }

            case 1: {
                return handleOneKeyValue(queries, 0, exchange, offset, count);
            }


            case 2: {
                return handleOneKeyValue(queries, 0, exchange, offset, count) && handleOneKeyValue(queries, 1, exchange, offset, count);
            }

            default: {
                sendBadRequest(exchange);
                return false;
            }
        }
    }

    private void sendBadRequest(HttpExchange exchange) throws IOException {
        exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
        writeResponse(exchange, "");
    }

    /**
     * @return true - if ok, false = send request
     */
    private boolean handleOneKeyValue(String[] queries, int indexInQueries, HttpExchange exchange, AtomicInteger offset, AtomicInteger count) throws IOException {
        String[] keyValue = queries[indexInQueries].split("=");
        switch (keyValue.length) {
            case 0: {
                sendBadRequest(exchange);
                return false;
            }

            case 1: {
                String key = keyValue[0];
                if (key.equals("count") || (key.equals("offset"))) {
                    break;
                } else {
                    sendBadRequest(exchange);
                    return false;
                }
            }

            case 2: {
                String key = keyValue[0];
                String value = keyValue[1];
                int intValue;
                try {
                    intValue = Integer.parseInt(value);
                    if (key.equals("count")) {
                        if (intValue <= 0 || intValue > 100) {
                            sendBadRequest(exchange);
                            return false;
                        } else {
                            count.getAndSet(intValue);
                        }
                    } else if (key.equals("offset")) {
                        offset.getAndSet(intValue);
                    }
                } catch (IndexOutOfBoundsException ex) {
                    sendBadRequest(exchange);
                    return false;
                }
                break;
            }

            default: {
                sendBadRequest(exchange);
                return false;
            }
        }

        return true;
    }


    private ExchangeClass checkRequestForGettingAllMessages(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        //405
        if (!requestMethod.equals("GET")) {
            exchange.sendResponseHeaders(METHOD_NOT_ALLOWED.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }
        return new ExchangeClass(exchange, true, null);
    }

    private ExchangeClass checkRequestForSendingMessage(HttpExchange exchange) throws IOException {
        String requestMethod = exchange.getRequestMethod();
        //405
        if (!requestMethod.equals("POST")) {
            exchange.sendResponseHeaders(METHOD_NOT_ALLOWED.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        Headers requestHeaders = exchange.getRequestHeaders();
        if (!requestHeaders.containsKey("Content-type")) {
            exchange.sendResponseHeaders(NOT_ACCEPTABLE.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        List<String> stringList = requestHeaders.get("Content-type");
        if (!stringList.contains("application/json") || stringList.size() != 1) {
            exchange.sendResponseHeaders(BAD_REQUEST.getCode(), 0);
            return new ExchangeClass(exchange, false, null);
        }

        return new ExchangeClass(exchange, true, null);
    }


    @Override
    public ExchangeClass validateRequest(HttpExchange exchange) {
        return null;
    }

}

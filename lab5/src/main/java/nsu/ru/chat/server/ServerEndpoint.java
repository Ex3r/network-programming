package nsu.ru.chat.server;

import com.google.gson.Gson;
import nsu.ru.chat.internal.MessageDecoder;
import nsu.ru.chat.internal.MessageTypes;
import nsu.ru.chat.model.Message;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import static nsu.ru.chat.internal.MessageTypes.*;


@javax.websocket.server.ServerEndpoint(
        value = "/chat/{user}/",
        decoders = {MessageDecoder.class}
)

public class ServerEndpoint {
    private static final String SERVER = "SERVER";
    private static final Set<String> USERS = new ConcurrentSkipListSet<>();
    private static final Map<String, Message> messages = new LinkedHashMap<>();
    private static final Gson gson = new Gson();
    private String user;
    private Session currentSession;
    private boolean dupUserDetected;

    @OnOpen
    public void userConnectedCallback(@PathParam("user") String user, Session s) {
        if (USERS.contains(user)) {
            try {
                dupUserDetected = true;
                String id = UUID.randomUUID().toString();
                Message errorMessage = new Message(id, ERROR, SERVER, "Username " + user + " has been taken. Retry with a different name");
                //messages.put(id, errorMessage);
                s.getBasicRemote().sendText(gson.toJson(errorMessage));
                s.close();
                return;
            } catch (IOException ex) {
                Logger.getLogger(ServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        this.currentSession = s;
        s.getUserProperties().put("user", user);
        this.user = user;
        USERS.add(user);

        sendGreetingToNewUserWhoJustConnected();
        notifyOthersAboutNewUser();
    }

    private void sendGreetingToNewUserWhoJustConnected() {
        try {
            String id = UUID.randomUUID().toString();
            String historyOfMessages = returnHistoryOfMessages();
            Message message = new Message(id, GREET_USER, SERVER, String.format("Glad to see you, %s!\n%s", user, historyOfMessages));
            currentSession.getBasicRemote().sendText((gson.toJson(message)));
        } catch (Exception ex) {
            Logger.getLogger(ServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String returnHistoryOfMessages() {
        StringBuilder history = new StringBuilder();
        Collection<Message> values = messages.values();
        int size = values.size();
        int startIndex = 0;
        if (size > 10) {
            startIndex = size - 10;
        }
        Message[] messages = values.toArray(new Message[values.size()]);
        for (int i = startIndex; i < size; i++) {
            String oneMessage = String.format("%s   :    %s\n", messages[i].getAuthor(), messages[i].getMsg());
            history.append(oneMessage);
        }
        return history.toString();
    }

    private void notifyOthersAboutNewUser() {
        String id = UUID.randomUUID().toString();
        Message message = new Message(id, NOTIFY_ABOUT_NEW, SERVER, "New user joined --- " + user);
        currentSession.getOpenSessions().stream()
                .filter((sn) -> !sn.getUserProperties().get("user").equals(this.user))
                .forEach((sn) -> sn.getAsyncRemote().sendText(gson.toJson(message)));
    }


    @OnMessage
    public void msgReceived(Message message, Session s) {
        String id = UUID.randomUUID().toString();
        message.setId(id);
        if (!message.getType().equals(MessageTypes.USERS)) {
            messages.put(id, message);
        }
        /*if (message.getMsg().equals(LOGOUT_MSG)) {
            try {
                processLogout();
                s.close();
                return;
            } catch (IOException ex) {
                Logger.getLogger(ServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            }
        }*/

        if (message.getType().equals(USUAL)) {
            s.getOpenSessions().stream()
                    .filter((session) -> !session.getUserProperties().get("user").equals(user))
                    .forEach((session) -> session.getAsyncRemote().sendText(gson.toJson(message)));
            return;
        }

        if (message.getType().equals(MessageTypes.USERS)) {
            StringBuilder listUsers = new StringBuilder();
            for (String user : USERS) {
                if (!user.equals(this.user)) {
                    listUsers.append(user).append("\n");
                }
            }
            Message msg = new Message(id, MessageTypes.USERS, SERVER, listUsers.toString());
            System.out.println(msg.getMsg());
            try {
                currentSession.getBasicRemote().sendText((gson.toJson(msg)));
            } catch (IOException e) {
                Logger.getLogger(ServerEndpoint.class.getName()).log(Level.SEVERE, null, e);
            }
        }


    }

    @OnClose
    public void onCloseCallback() {
        if (!dupUserDetected) {
            processLogout();
        }

    }

    private void processLogout() {
        try {
            String id = UUID.randomUUID().toString();
            Message message = new Message(id, LOGOUT, SERVER, "User " + user + " has logged out");
            //messages.put(id, message);
            USERS.remove(this.user);
            currentSession.getOpenSessions().stream()
                    .filter(Session::isOpen)
                    .forEach((session) -> session.getAsyncRemote().sendText(gson.toJson(message)));

        } catch (Exception ex) {
            Logger.getLogger(ServerEndpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

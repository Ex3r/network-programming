package nsu.ru.chat.client;

import nsu.ru.chat.internal.MessageDecoder;
import nsu.ru.chat.internal.MessageEncoder;
import nsu.ru.chat.model.Message;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;

import static nsu.ru.chat.internal.MessageTypes.*;

@javax.websocket.ClientEndpoint(
        encoders = {MessageEncoder.class,
        },

        decoders = {MessageDecoder.class,

        })
public class ClientEndpoint {

    @OnOpen
    public void onOpen() {
        System.out.println("on open");
    }

    @OnMessage
    public void onMessage(Message message) {
        switch (message.getType()) {
            case USUAL: {
                System.out.println(String.format("%s : %s",
                        message.getAuthor(), message.getMsg()));
                break;
            }
            case GREET_USER: {
                System.out.println("Вы успешно вошли в чат");
                System.out.println(message.getMsg());
                break;
            }

            case NOTIFY_ABOUT_NEW: {
                System.out.println(message.getMsg());
                break;
            }

            case LOGOUT: {
                System.out.println(message.getMsg());
                break;
            }
            case ERROR: {
                ClientInfo.resetInfo();
                System.out.println(message.getMsg());
                break;
            }

            case USERS: {
                if (message.getMsg().equals("")) {
                    System.out.println("Вы один в чате :(");
                } else {
                    System.out.println("Список активных участников чата:");
                    System.out.println(message.getMsg());
                }

                break;
            }
        }

    }


}
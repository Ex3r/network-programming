package ru.ccfit.terekhov.other;

public class Constatnts {
    public static final int  BUFFER_SIZE = 10000;
    public static final Integer AMOUNT_OF_TRY_TO_SEND_PACKET = 100;
    public static final String DELIMETER_FOR_IP_ANDPORT = "_";
    public static final String DELIMETER_IN_PACKET = "\n";
    public static final String NODE_NAME_PARAMETER = "Node name";
    public static final String PACKET_LOSS_PARAMETER = "Packet loss";
    public static final String SELF_PORT = "Self port";
    public static final String PARENT_IP = "Parent ip";
    public static final String PARENT_PORT = "Parent port";


    /*Message types*/
    public static final Integer HELLO_MSG = 1;
    public static final Integer USUAL_MSG = 2;
    public static final Integer PING_MSG = 3;
    public static final Integer ACK_MSG = 4;
    public static final Integer DISCONNECT_MSG = 5;


    public static final Integer DEAD_VALUE = 10;
    public static final Integer PING_INTERVAL = 1000;
}

import org.apache.commons.cli.CommandLine;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;
import java.util.logging.Logger;

public class Server implements Runnable {
    private static final Logger logger = Logger.getLogger (Server.class.getName ());
    private static final String UPLOADS_DIR_PATH = "uploads";
    private ServerSocket serverSocket;
    private List<Receiver> clients;

    public Server(int port) {
        clients = new ArrayList<> ();
        try {
            this.serverSocket = new ServerSocket (port);
        } catch (IOException e) {
            logger.severe (e.getMessage ());
        }

        try {
            Utilits.createFolder (UPLOADS_DIR_PATH);
        } catch (IOException e) {
            logger.severe (e.getMessage ());
        }
    }

    public void run() {
        while (!Thread.currentThread ().isInterrupted ()) {
            try {
                Receiver receiver = new Receiver (serverSocket.accept ());
                clients.add (receiver);
                logger.info ("New client " + receiver.getClientSocket ().getInetAddress ());
                receiver.run ();

            } catch (IOException e) {
                logger.severe (e.getMessage ());
            }
        }
    }

    public static void main(String[] args) {
        Map<String, String> parametersForOptions = new LinkedHashMap<> ();
        parametersForOptions.put (Utilits.PORT, "port");
        CommandLineParserCustom commandParser = new CommandLineParserCustom (parametersForOptions);
        CommandLine commandParserArgs = commandParser.getArgs (args);
        if (commandParserArgs == null) {
            return;
        }

        String portStr = commandParserArgs.getOptionValue (Utilits.PORT);
        int port = Integer.parseInt (portStr);
        Server server = new Server (port);
        Thread serverThread = new Thread (server);

        serverThread.start ();
    }

}

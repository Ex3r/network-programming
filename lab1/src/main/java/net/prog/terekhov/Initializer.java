/*
 * Developed by Terekhov Roman on 18.09.18 1:34.
 * Last modified 18.09.18 1:17.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import org.apache.commons.cli.CommandLine;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Logger;

public class Initializer {
    private final static String buffer_size = "BUFFER_SIZE";
    private final static String IP = "ip";
    private final static String PORT = "port";
    private final static Logger logger = Logger.getLogger (Initializer.class.getName ());

    public static void main(String[] args) throws IOException {
        CommandLine commandArgs = CommandLineParserCustom.getArgs (args);
        if (commandArgs == null) {
            return;
        }
        String ip = commandArgs.getOptionValue (IP);
        String portStr = commandArgs.getOptionValue (PORT);
        int port = Integer.parseInt (portStr);
        LoaderProperties loaderProperties = new LoaderProperties ("config.properties");
        Properties properties = loaderProperties.getPropValues ();

        String bufferSizeStr = properties.getProperty (buffer_size);
        int bufferSize = Integer.parseInt (bufferSizeStr);
        boolean isMulticastAddress = false;
        try {
            isMulticastAddress = MulticastValidator.validateIp (ip);
        } catch (UnknownHostException e) {
            System.out.println ("ERROR: " + e.getLocalizedMessage ());
        }
        if (!isMulticastAddress) {
            System.out.printf ("This ip address: %s is not multicast%n", ip);
            return;
        }

        MulticastSender sender = new MulticastSender (ip, port, bufferSize);
        MulticastReceiver receiver = new MulticastReceiver (port, ip, bufferSize);
        PacketHandler packetHandler = new PacketHandler ();
        Thread senderThread = new Thread (sender);
        Thread packetHandlerThread = new Thread (packetHandler);
        senderThread.start ();
        packetHandlerThread.start ();
        while (!Thread.currentThread ().isInterrupted ()) {
            DatagramPacket packet = receiver.receive ();
            packetHandler.handlePacket (packet);
        }

    }
}

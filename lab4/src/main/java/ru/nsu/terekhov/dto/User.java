package ru.nsu.terekhov.dto;

public class User {
    private int id;
    private String username;
    private Boolean online;
    private String token;

    public User(int id) {
        this.id = id;
    }

    public User(int id, String username, Boolean online, String token) {
        this.id = id;
        this.username = username;
        this.online = online;
        this.token = token;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getOnline() {
        return online;
    }

    public String getToken() {
        return token;
    }
}

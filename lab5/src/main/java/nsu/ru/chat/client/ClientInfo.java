package nsu.ru.chat.client;

import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.Session;

public class ClientInfo {
    private static boolean logined = false;
    private static ClientManager client;
    private static Session session;
    private static String userName;


    public static Session getSession() {
        return session;
    }

    public static void setSession(Session session) {
        ClientInfo.session = session;
    }

    public static boolean isLogined() {
        return logined;
    }

    public static void setLogined(boolean logined) {
        ClientInfo.logined = logined;
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        ClientInfo.userName = userName;
    }

    public static ClientManager getClient() {

        return client;
    }

    public static void setClient(ClientManager client) {
        ClientInfo.client = client;
    }

    public static void resetInfo() {
        logined = false;
        client = null;
        session = null;
        userName = "";
    }
}

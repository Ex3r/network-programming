/*
 * Developed by Terekhov Roman on 18.09.18 1:34.
 * Last modified 18.09.18 1:34.
 * Copyright (c) 2018. All rights reserved.
 */

package net.prog.terekhov;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Logger;

public class MulticastReceiver {
    private static final Logger logger = Logger.getLogger (MulticastReceiver.class.getName ());
    private final int port;
    private final String multicastGroup;
    private int bufferSize;
    private final MulticastSocket socket;
    private final InetAddress address;
    private byte[] buf;
    private DatagramPacket packet;

    MulticastReceiver(int port, String multicastGroup, int bufferSize) throws IOException {
        this.port = port;
        this.multicastGroup = multicastGroup;
        this.bufferSize = bufferSize;
        socket = new MulticastSocket (port);
        address = InetAddress.getByName (multicastGroup);
        socket.joinGroup (address);
        buf = new byte[bufferSize];
        packet = new DatagramPacket (buf, buf.length);
    }

    public DatagramPacket receive() throws IOException {
        socket.receive (packet);
        return packet;
    }

    public void close() throws IOException {
        socket.leaveGroup (address);
        socket.close ();
    }
}
